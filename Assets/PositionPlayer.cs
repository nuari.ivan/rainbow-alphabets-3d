using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PositionPlayer : MonoBehaviour
{
    public GameObject RedMonster;
    public Transform PositionPlayers;

    [Header("Total Leds")]
    public GameObject[] Leds;
    internal int currentled;

    void Start()
    {
        RedMonster.transform.position = PositionPlayers.transform.position;
    }
    public void ActivateLed()
    {
        Leds[currentled].gameObject.SetActive(true);
    }
}
