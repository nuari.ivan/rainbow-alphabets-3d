Shader "Hidden/ProBuilder/VertexPicker"
{
  Properties
  {
  }
  SubShader
  {
    Tags
    { 
      "DisableBatching" = "true"
      "IGNOREPROJECTOR" = "true"
      "ProBuilderPicker" = "VertexPass"
      "RenderType" = "Transparent"
    }
    Pass // ind: 1, name: Vertices
    {
      Name "Vertices"
      Tags
      { 
        "DisableBatching" = "true"
        "IGNOREPROJECTOR" = "true"
        "ProBuilderPicker" = "VertexPass"
        "RenderType" = "Transparent"
      }
      Cull Off
      Offset -1, -1
      // m_ProgramMask = 6
      CGPROGRAM
      //#pragma target 4.0
      
      #pragma vertex vert
      #pragma fragment frag
      
      #include "UnityCG.cginc"
      
      
      #define CODE_BLOCK_VERTEX
      
      
      uniform float4 _ScreenParams;
      
      uniform float4 unity_ObjectToWorld[4];
      
      uniform float4 UNITY_MATRIX_P[4];
      
      uniform float4 unity_MatrixV[4];
      
      
      
      struct appdata_t
      {
          
          float4 vertex : POSITION0;
          
          float4 color : COLOR0;
          
          float2 texcoord : TEXCOORD0;
          
          float2 texcoord1 : TEXCOORD1;
      
      };
      
      
      struct OUT_Data_Vert
      {
          
          float2 texcoord : TEXCOORD0;
          
          float4 color : COLOR0;
          
          float4 vertex : SV_POSITION;
      
      };
      
      
      struct v2f
      {
          
          float4 color : COLOR0;
      
      };
      
      
      struct OUT_Data_Frag
      {
          
          float4 color : SV_Target0;
      
      };
      
      
      float4 u_xlat0;
      
      float4 u_xlat1;
      
      float u_xlat6;
      
      OUT_Data_Vert vert(appdata_t in_v)
      {
          
          u_xlat0 = in_v.vertex.yyyy * unity_ObjectToWorld[1];
          
          u_xlat0 = unity_ObjectToWorld[0] * in_v.vertex.xxxx + u_xlat0;
          
          u_xlat0 = unity_ObjectToWorld[2] * in_v.vertex.zzzz + u_xlat0;
          
          u_xlat0 = u_xlat0 + unity_ObjectToWorld[3];
          
          u_xlat1.xyz = u_xlat0.yyy * unity_MatrixV[1].xyz;
          
          u_xlat1.xyz = unity_MatrixV[0].xyz * u_xlat0.xxx + u_xlat1.xyz;
          
          u_xlat0.xyz = unity_MatrixV[2].xyz * u_xlat0.zzz + u_xlat1.xyz;
          
          u_xlat0.xyz = unity_MatrixV[3].xyz * u_xlat0.www + u_xlat0.xyz;
          
          u_xlat6 = (-UNITY_MATRIX_P[3].w) + 1.0;
          
          u_xlat1.x = u_xlat6 * -0.0400000215 + 0.99000001;
          
          u_xlat0.xyz = u_xlat0.xyz * u_xlat1.xxx;
          
          u_xlat1 = u_xlat0.yyyy * UNITY_MATRIX_P[1];
          
          u_xlat1 = UNITY_MATRIX_P[0] * u_xlat0.xxxx + u_xlat1;
          
          u_xlat1 = UNITY_MATRIX_P[2] * u_xlat0.zzzz + u_xlat1;
          
          u_xlat1 = u_xlat1 + UNITY_MATRIX_P[3];
          
          u_xlat0.xy = u_xlat1.xy / u_xlat1.ww;
          
          u_xlat0.xy = u_xlat0.xy * float2(0.5, 0.5) + float2(0.5, 0.5);
          
          u_xlat1.xy = in_v.texcoord1.xy * float2(3.5, 3.5);
          
          u_xlat0.xy = u_xlat0.xy * _ScreenParams.xy + u_xlat1.xy;
          
          u_xlat0.xy = u_xlat0.xy / _ScreenParams.xy;
          
          u_xlat0.xy = u_xlat0.xy + float2(-0.5, -0.5);
          
          u_xlat0.xy = u_xlat1.ww * u_xlat0.xy;
          
          out_v.vertex.xy = u_xlat0.xy + u_xlat0.xy;
          
          out_v.vertex.z = (-u_xlat6) * 9.99999975e-05 + u_xlat1.z;
          
          out_v.vertex.w = u_xlat1.w;
          
          out_v.texcoord.xy = in_v.texcoord.xy;
          
          out_v.color = in_v.color;
          
          return;
      
      }
      
      
      #define CODE_BLOCK_FRAGMENT
      
      
      
      OUT_Data_Frag frag(v2f in_f)
      {
          
          out_f.color = in_f.color;
          
          return;
      
      }
      
      
      ENDCG
      
    } // end phase
  }
  FallBack Off
}
