Shader "Dacodelaac/Lit/Basic_Outline"
{
  Properties
  {
    [Header(Base)] [Space] _Color ("Color", Color) = (1,1,1,1)
    _HColor ("Highlight Color", Color) = (1,1,1,1)
    _SColor ("Shadow Color", Color) = (0.2,0.2,0.2,1)
    _MainTex ("Main Texture (RGB) Spec/MatCap Mask (A) ", 2D) = "white" {}
    [Header(Ramp)] [Space] [Toggle(RAMP)] _UseRamp ("Enable", float) = 0
    _RampThreshold ("Ramp Threshold", Range(0, 1)) = 0.3
    _RampSmooth ("Ramp Smoothing", Range(0.01, 1)) = 0.01
    [Header(Emission)] [Space] [Toggle(EMISSION)] _UseEmission ("Enable", float) = 0
    [HDR] _Emission ("Emission Color", Color) = (0,0,0,1)
    _EmissionRate ("Emission Rate", float) = 0
    [Header(Normal Map)] [Space] [Toggle(BUMP)] _UseBump ("Enable", float) = 0
    [NoScaleOffset] _BumpMap ("Normal map (RGB)", 2D) = "bump" {}
    [Header(Specular)] [Space] [Toggle(SPEC)] _UseSpec ("Enable", float) = 0
    [Toggle(SPEC_TOON)] _UseSpecToon ("Toon", float) = 0
    _SpecColor ("Specular Color", Color) = (0.5,0.5,0.5,1)
    _Shininess ("Shininess", Range(0.01, 2)) = 0.1
    _SpecSmooth ("Smoothness", Range(0, 1)) = 0.05
    [Header(Rim)] [Space] [Toggle(RIM)] _UseRim ("Enable", float) = 0
    _RimColor ("Rim Color", Color) = (0.8,0.8,0.8,0.6)
    _RimMin ("Rim Min", Range(0, 1)) = 0.5
    _RimMax ("Rim Max", Range(0, 1)) = 1
    [Header(Relfection)] [Space] [Toggle(MATCAP)] _UseMatCap ("Enable", float) = 0
    _MatCap ("MatCap (RGB)", 2D) = "black" {}
    [Header(VerticalFog)] [Space] [Toggle(VERTICAL_FOG)] _UseVerticalFog ("Enable", float) = 0
    _FogColor ("Fog Color", Color) = (0,0,0,0)
    _FogHeight ("Fog Height", float) = 0
    _FogFade ("Fog Fade", float) = 0
    [Header(Outline)] [Space] _Outline ("Outline Width", float) = 0.02
    _OutlineType ("Outline type", float) = 0
    _OutlineColor ("Outline Color", Color) = (0.2,0.2,0.2,1)
    [Enum(UnityEngine.Rendering.CullMode)] _Culling ("Cull", float) = 2
  }
  SubShader
  {
    Tags
    { 
      "RenderType" = "Opaque"
    }
    LOD 200
    Pass // ind: 1, name: FORWARD
    {
      Name "FORWARD"
      Tags
      { 
        "LIGHTMODE" = "FORWARDBASE"
        "RenderType" = "Opaque"
        "SHADOWSUPPORT" = "true"
      }
      LOD 200
      Cull Off
      Stencil
      { 
        Ref 3
        ReadMask 255
        WriteMask 255
        Pass Replace
        Fail Keep
        ZFail Keep
        PassFront Replace
        FailFront Keep
        ZFailFront Keep
        PassBack Replace
        FailBack Keep
        ZFailBack Keep
      } 
      // m_ProgramMask = 6
      CGPROGRAM
      #pragma multi_compile DIRECTIONAL
      //#pragma target 4.0
      
      #pragma vertex vert
      #pragma fragment frag
      
      #include "UnityCG.cginc"
      
      
      #define CODE_BLOCK_VERTEX
      
      
      uniform float4 unity_ObjectToWorld[4];
      
      uniform float4 unity_WorldToObject[4];
      
      uniform float4 unity_MatrixVP[4];
      
      uniform float4 _MainTex_ST;
      
      uniform float4 _WorldSpaceLightPos0;
      
      uniform float4 _LightColor0;
      
      uniform float4 _Color;
      
      uniform float4 _HColor;
      
      uniform float4 _SColor;
      
      uniform sampler2D _MainTex;
      
      
      
      struct appdata_t
      {
          
          float4 vertex : POSITION0;
          
          float3 normal : NORMAL0;
          
          float4 texcoord : TEXCOORD0;
      
      };
      
      
      struct OUT_Data_Vert
      {
          
          float2 texcoord : TEXCOORD0;
          
          float3 texcoord1 : TEXCOORD1;
          
          float3 texcoord2 : TEXCOORD2;
          
          float3 texcoord3 : TEXCOORD3;
          
          float4 texcoord5 : TEXCOORD5;
          
          float4 vertex : SV_POSITION;
      
      };
      
      
      struct v2f
      {
          
          float2 texcoord : TEXCOORD0;
          
          float3 texcoord1 : TEXCOORD1;
          
          float3 texcoord3 : TEXCOORD3;
      
      };
      
      
      struct OUT_Data_Frag
      {
          
          float4 color : SV_Target0;
      
      };
      
      
      float4 u_xlat0;
      
      float4 u_xlat1;
      
      float u_xlat6;
      
      OUT_Data_Vert vert(appdata_t in_v)
      {
          
          u_xlat0 = in_v.vertex.yyyy * unity_ObjectToWorld[1];
          
          u_xlat0 = unity_ObjectToWorld[0] * in_v.vertex.xxxx + u_xlat0;
          
          u_xlat0 = unity_ObjectToWorld[2] * in_v.vertex.zzzz + u_xlat0;
          
          u_xlat1 = u_xlat0 + unity_ObjectToWorld[3];
          
          out_v.texcoord2.xyz = unity_ObjectToWorld[3].xyz * in_v.vertex.www + u_xlat0.xyz;
          
          u_xlat0 = u_xlat1.yyyy * unity_MatrixVP[1];
          
          u_xlat0 = unity_MatrixVP[0] * u_xlat1.xxxx + u_xlat0;
          
          u_xlat0 = unity_MatrixVP[2] * u_xlat1.zzzz + u_xlat0;
          
          out_v.vertex = unity_MatrixVP[3] * u_xlat1.wwww + u_xlat0;
          
          u_xlat0.xy = in_v.texcoord.xy * _MainTex_ST.xy + _MainTex_ST.zw;
          
          out_v.texcoord.xy = u_xlat0.xy;
          
          u_xlat0.x = dot(in_v.normal.xyz, unity_WorldToObject[0].xyz);
          
          u_xlat0.y = dot(in_v.normal.xyz, unity_WorldToObject[1].xyz);
          
          u_xlat0.z = dot(in_v.normal.xyz, unity_WorldToObject[2].xyz);
          
          u_xlat6 = dot(u_xlat0.xyz, u_xlat0.xyz);
          
          u_xlat6 = inversesqrt(u_xlat6);
          
          out_v.texcoord1.xyz = float3(u_xlat6) * u_xlat0.xyz;
          
          out_v.texcoord3.xyz = float3(0.0, 0.0, 0.0);
          
          out_v.texcoord5 = float4(0.0, 0.0, 0.0, 0.0);
          
          return;
      
      }
      
      
      #define CODE_BLOCK_FRAGMENT
      
      
      
      float3 u_xlat16_0;
      
      float3 u_xlat16_1;
      
      float3 u_xlat16_2;
      
      float3 u_xlat16_3;
      
      float3 u_xlat16_4;
      
      OUT_Data_Frag frag(v2f in_f)
      {
          
          u_xlat16_0.x = dot(in_f.texcoord1.xyz, in_f.texcoord1.xyz);
          
          u_xlat16_0.x = inversesqrt(u_xlat16_0.x);
          
          u_xlat16_0.xyz = u_xlat16_0.xxx * in_f.texcoord1.xyz;
          
          u_xlat16_0.x = dot(u_xlat16_0.xyz, _WorldSpaceLightPos0.xyz);
          
          u_xlat16_0.x = max(u_xlat16_0.x, 0.0);
          
          u_xlat16_4.xyz = (-_HColor.xyz) + _SColor.xyz;
          
          u_xlat16_4.xyz = _SColor.www * u_xlat16_4.xyz + _HColor.xyz;
          
          u_xlat16_1.xyz = (-u_xlat16_4.xyz) + _HColor.xyz;
          
          u_xlat16_0.xyz = u_xlat16_0.xxx * u_xlat16_1.xyz + u_xlat16_4.xyz;
          
          u_xlat16_2.xyz = texture(_MainTex, in_f.texcoord.xy).xyz;
          
          u_xlat16_1.xyz = u_xlat16_2.xyz * _Color.xyz;
          
          u_xlat16_3.xyz = u_xlat16_1.xyz * _LightColor0.xyz;
          
          u_xlat16_0.xyz = u_xlat16_0.xyz * u_xlat16_3.xyz;
          
          out_f.color.xyz = u_xlat16_1.xyz * in_f.texcoord3.xyz + u_xlat16_0.xyz;
          
          out_f.color.w = 1.0;
          
          return;
      
      }
      
      
      ENDCG
      
    } // end phase
    Pass // ind: 2, name: 
    {
      Tags
      { 
        "RenderType" = "Opaque"
      }
      LOD 200
      Cull Front
      Stencil
      { 
        Ref 3
        ReadMask 255
        WriteMask 255
        Pass Keep
        Fail Keep
        ZFail Keep
        PassFront Keep
        FailFront Keep
        ZFailFront Keep
        PassBack Keep
        FailBack Keep
        ZFailBack Keep
      } 
      // m_ProgramMask = 6
      CGPROGRAM
      //#pragma target 4.0
      
      #pragma vertex vert
      #pragma fragment frag
      
      #include "UnityCG.cginc"
      
      
      #define CODE_BLOCK_VERTEX
      
      
      uniform float4 unity_ObjectToWorld[4];
      
      uniform float4 unity_MatrixVP[4];
      
      uniform float _Outline;
      
      uniform float _OutlineType;
      
      uniform float4 _OutlineColor;
      
      
      
      struct appdata_t
      {
          
          float4 vertex : POSITION0;
          
          float3 normal : NORMAL0;
      
      };
      
      
      struct OUT_Data_Vert
      {
          
          float4 vertex : SV_POSITION;
      
      };
      
      
      struct v2f
      {
          
          float4 vertex : Position;
      
      };
      
      
      struct OUT_Data_Frag
      {
          
          float4 color : SV_Target0;
      
      };
      
      
      float4 u_xlat0;
      
      float3 u_xlat16_0;
      
      float4 u_xlat1;
      
      float4 u_xlat2;
      
      float3 u_xlat16_2;
      
      int u_xlatb3;
      
      OUT_Data_Vert vert(appdata_t in_v)
      {
          
          u_xlat16_0.xyz = in_v.normal.xyz * float3(_Outline);
          
          u_xlat16_0.xyz = u_xlat16_0.xyz * float3(0.00999999978, 0.00999999978, 0.00999999978) + in_v.vertex.xyz;
          
          u_xlat1 = u_xlat16_0.yyyy * unity_ObjectToWorld[1];
          
          u_xlat1 = unity_ObjectToWorld[0] * u_xlat16_0.xxxx + u_xlat1;
          
          u_xlat0 = unity_ObjectToWorld[2] * u_xlat16_0.zzzz + u_xlat1;
          
          u_xlat0 = u_xlat0 + unity_ObjectToWorld[3];
          
          u_xlat1 = u_xlat0.yyyy * unity_MatrixVP[1];
          
          u_xlat1 = unity_MatrixVP[0] * u_xlat0.xxxx + u_xlat1;
          
          u_xlat1 = unity_MatrixVP[2] * u_xlat0.zzzz + u_xlat1;
          
          u_xlat0 = unity_MatrixVP[3] * u_xlat0.wwww + u_xlat1;
          
          u_xlat16_2.x = _Outline * 0.00999999978 + 1.0;
          
          u_xlat16_2.xyz = u_xlat16_2.xxx * in_v.vertex.xyz;
          
          u_xlat1 = u_xlat16_2.yyyy * unity_ObjectToWorld[1];
          
          u_xlat1 = unity_ObjectToWorld[0] * u_xlat16_2.xxxx + u_xlat1;
          
          u_xlat1 = unity_ObjectToWorld[2] * u_xlat16_2.zzzz + u_xlat1;
          
          u_xlat1 = u_xlat1 + unity_ObjectToWorld[3];
          
          u_xlat2 = u_xlat1.yyyy * unity_MatrixVP[1];
          
          u_xlat2 = unity_MatrixVP[0] * u_xlat1.xxxx + u_xlat2;
          
          u_xlat2 = unity_MatrixVP[2] * u_xlat1.zzzz + u_xlat2;
          
          u_xlat1 = unity_MatrixVP[3] * u_xlat1.wwww + u_xlat2;
          
          #ifdef UNITY_ADRENO_ES3
          u_xlatb3 = (_OutlineType==0.0);
          
          #else
          u_xlatb3 = _OutlineType==0.0;
          
          #endif
          out_v.vertex = (int(u_xlatb3)) ? u_xlat0 : u_xlat1;
          
          return;
      
      }
      
      
      #define CODE_BLOCK_FRAGMENT
      
      
      
      OUT_Data_Frag frag(v2f in_f)
      {
          
          out_f.color = _OutlineColor;
          
          return;
      
      }
      
      
      ENDCG
      
    } // end phase
  }
  FallBack "Diffuse"
}
