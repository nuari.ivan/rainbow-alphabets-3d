using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.UI;
using TMPro;

public class CharactersMoving : MonoBehaviour
{
    [Header("Manager AI")]
    private NavMeshAgent CurrentAgent;
    private Rigidbody thisRb;

    [Header("Manager Controller")]
    public ManagerLeaderBoard LeaderController;
    public GameLauncherController Launcher;
    public ManagerList ListController;
    public BooleanManager BoolM;

    [Header("Animator Controller")]
    public Animator CurrentAnimator;

    [Header("Name Manager")]
    public TextMeshProUGUI CurrentName;

    [Header("Manager CurrentBox")]
    public GameObject[] CurrentBox;
    public GameObject[] Boxes;
    public GameObject[] CurrentFoods;
    public GameObject[] Foods;
    public GameObject[] CurrentLeds;
    public GameObject[] Leds;
    public GameObject[] FollowBattrey;
    public GameObject[] Lamps;
    public GameObject[] CurrentLamps;


    [Header("Controller Messages")]
    public GameObject Message;
    public GameObject ContainerMessage;
    public GameObject BackStation;
    public GameObject ManagerFlexing;
    public GameObject BackstationLed;
    public GameObject BackstaationBattrey;
    public GameObject CurrentBattrey;
    public GameObject NBackStationLamps;

    [Header("Integer Manager")]
    internal int CurrentObject;
    internal int TotalCurrents;
    internal int CurrentObjectFood;
    internal int CurrentObjectLeds;
    internal int CurrentObjectBattrey;
    internal int CurrentObjectLamps;

    [Header("Boolean Manager")]
    internal bool CheckMovePP = false;
    internal bool FixObjectTaker = false;
    internal bool TakingBox = true;
    internal bool BackBox = true;
    internal bool BoxAreTaked = false;
    internal bool FixingSpawningBox = true;
    internal bool CheckingFood = true;
    internal bool BackToStation = false;
    internal bool FixingTakingFood = true;
    internal bool FixingLeds = true;
    internal bool CheckMovingCharacter = true;
    internal bool BackTostationLeds = false;
    internal bool FixingBattrey = true;
    internal bool BattreyFollow = true;
    internal bool BacktoStationBattrey = false;
    internal bool SpawningFixingLamps = true;
    internal bool BackToLamps = false;
    internal bool FollowLamps = true;

    [Header("Nmaes Strings")]
    public string[] last = new string[] { "avon", "bage", "beck", "borne", "borough", "bourne", "bridge", "brook", "brough", "bury", "by", "castle",
            "cester", "chester", "combe", "den", "ditch", "don", "down", "ey", "field", "ford", "grove", "hall", "ham", "hampton", "head", "lake",
            "ley", "ling", "low", "mere", "moor", "nell", "ney", "over", "port", "shot", "side", "smith", "sted", "stoke", "thorne", "ton", "tree",
            "wang", "well", "wich", "wick", "wold", "wood", "worth" };

    void Awake()
    {
        ListController = GameObject.FindGameObjectWithTag("ManagerLeaderBoard").GetComponent<ManagerList>();
        Launcher = GameObject.Find("GameLauncher").GetComponent<GameLauncherController>();
        BoolM = GameObject.Find("BooleanManager").GetComponent<BooleanManager>();
    }
    void Start()
    {
        CurrentAgent = GetComponent<NavMeshAgent>();
        thisRb = GetComponent<Rigidbody>();
        ContainerMessage = GameObject.Find("GamePopup");
        CurrentName.text = last[Random.Range(0 , last.Length)];
        foreach (GameObject list in ListController.ListAllNames)
        {
            if(list.name + "(Clone)" == this.gameObject.name)
            {
                LeaderController = list.gameObject.GetComponent<ManagerLeaderBoard>();
            }
        }
        LeaderController.playerName[0].text = CurrentName.text;
        LeaderController.playerName[1].text = CurrentName.text;
        StartCoroutine(CheckingStart());
        StartCoroutine(SpawningMessage());
    }
    void FixedUpdate()
    {
        if (CheckMovePP == true && BoolM.LevelOne == true)
        {
            if(BackBox == true)
            {
                if (CurrentObject < CurrentBox.Length && CurrentBox[CurrentObject].gameObject != null)
                {
                    CurrentAgent.SetDestination(CurrentBox[CurrentObject].transform.position);
                    transform.LookAt(CurrentBox[CurrentObject].transform.position);
                }
                else
                {
                    CheckMovePP = false;
                    FixObjectTaker = true;
                    StopCoroutine(CheckingStart());
                    StartCoroutine(CheckingStart());
                }
            }
            if(BackBox == false)
            {
                CurrentAgent.SetDestination(Launcher.PositionBoxes[Launcher.CurrentActiveBox].transform.position);
                transform.LookAt(Launcher.PositionBoxes[Launcher.CurrentActiveBox].transform.position);
            }
            if (thisRb.velocity.magnitude > 0)
            {
                CurrentAnimator.Play("run");
            }
            else
            {
                CurrentAnimator.Play("idle");
            }
        }
        if(CheckMovePP == true && BoolM.LevelTwo == true)
        {
            if(CheckingFood == true)
            {
                CurrentFoods = GameObject.FindGameObjectsWithTag("Food");
                CurrentObjectFood = Random.Range(0 , CurrentFoods.Length);
                BackStation = GameObject.FindGameObjectWithTag("DropFood");
                ManagerFlexing = GameObject.Find("Goal_Food");
                CheckingFood = false;
            }
            if(CurrentObjectFood < CurrentFoods.Length)
            {
                if(CurrentFoods[CurrentObjectFood].gameObject == null)
                {
                    CheckingFood = true;
                }
                if(BackToStation == false && CurrentFoods[CurrentObjectFood].gameObject != null)
                {
                    CurrentAgent.SetDestination(CurrentFoods[CurrentObjectFood].transform.position);
                    transform.LookAt(CurrentFoods[CurrentObjectFood].transform.position);
                }
                else if(BackToStation == true)
                {
                    CurrentAgent.SetDestination(BackStation.transform.position);
                    transform.LookAt(BackStation.transform.position);
                }
            }
            if (thisRb.velocity.magnitude > 0)
            {
                CurrentAnimator.Play("run");
            }
            else
            {
                CurrentAnimator.Play("idle");
            }
        }
        if(CheckMovePP == true && BoolM.LevelThree == true)
        {
            if(CheckMovingCharacter == true)
            {
                CurrentLeds = GameObject.FindGameObjectsWithTag("Leds");
                BackstationLed = GameObject.FindGameObjectWithTag("LedsDrop");
                CurrentObjectLeds = Random.Range(0, CurrentLeds.Length);
                CheckMovingCharacter = false;
            }
            if (CurrentLeds[CurrentObjectLeds].gameObject == null)
            {
                CheckMovingCharacter = true;
            }
            if(BackTostationLeds == false && CurrentLeds[CurrentObjectLeds].gameObject != null)
            {
                CurrentAgent.SetDestination(CurrentLeds[CurrentObjectLeds].transform.position);
            }
            else if(BackTostationLeds == true)
            {
                CurrentAgent.SetDestination(BackstationLed.transform.position);
            }
            if (thisRb.velocity.magnitude > 0)
            {
                CurrentAnimator.Play("run");
            }
            else
            {
                CurrentAnimator.Play("idle");
            }
        }
        if(CheckMovePP == true && BoolM.LevelFour == true)
        {
            if(BattreyFollow == true)
            {
                FollowBattrey = GameObject.FindGameObjectsWithTag("Battrey");
                BackstaationBattrey = GameObject.FindGameObjectWithTag("BattreyDrop");
                CurrentObjectBattrey = Random.Range(0, FollowBattrey.Length);
                BattreyFollow = false;
            }
            if(FollowBattrey.Length == 0)
            {
                BattreyFollow = true;
            }
            else
            {
                if (BacktoStationBattrey == false && FollowBattrey[CurrentObjectBattrey].gameObject != null)
                {
                    CurrentAgent.SetDestination(FollowBattrey[CurrentObjectBattrey].transform.position);
                }
            }
            if(BacktoStationBattrey == true)
            {
                CurrentAgent.SetDestination(BackstaationBattrey.transform.position);
            }
            if (thisRb.velocity.magnitude > 0)
            {
                CurrentAnimator.Play("run");
            }
            else
            {
                CurrentAnimator.Play("idle");
            }
        }
        if(CheckMovePP == true && BoolM.LevelFive == true)
        {
            Lamps = GameObject.FindGameObjectsWithTag("Lamp");
            if (FollowLamps == true)
            {
                NBackStationLamps = GameObject.FindGameObjectWithTag("LampDrop");
                CurrentObjectLamps = Random.Range(0, Lamps.Length);
                FollowLamps = false;
            }
            if (Lamps.Length == 0)
            {
                FollowLamps = true;
            }
            else
            {
                //Debug.Log($"total Lamps = {Lamps.Length} , current object lamps = {CurrentObjectLamps}.");
                if(CurrentObjectLamps >= Lamps.Length)
                {
                    return;
                }
                if (BackToLamps == false && Lamps[CurrentObjectLamps].gameObject != null)
                {
                    CurrentAgent.SetDestination(Lamps[CurrentObjectLamps].transform.position);
                }
            }
            if(BackToLamps == true)
            {
                CurrentAgent.SetDestination(NBackStationLamps.transform.position);
            }
            if (thisRb.velocity.magnitude > 0)
            {
                CurrentAnimator.Play("run");
            }
            else
            {
                CurrentAnimator.Play("idle");
            }
        }
    }
    IEnumerator SpawningMessage()
    {
        yield return new WaitForSeconds(1.5f);
        (Instantiate(Message, ContainerMessage.transform.position, Message.transform.rotation) as GameObject).transform.SetParent(ContainerMessage.transform);
    }
    IEnumerator CheckingStart()
    {
        yield return new WaitForSeconds(1f);
        CurrentBox = GameObject.FindGameObjectsWithTag("Box");
        if(CheckMovePP == false)
        {
            CurrentObject = Random.Range(0, CurrentBox.Length);
        }
        if(FixObjectTaker == true)
        {
            CurrentObject = Random.Range(0, CurrentBox.Length);
            CheckMovePP = true;
        }
        yield return new WaitForSeconds(3f);
        StartCoroutine(CheckingStart());
    }
    void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Box") && BoolM.LevelOne == true)
        {
            if(TakingBox == true)
            {
                if (other.name == "X - Copy(Clone)")
                {
                    BoxAreTaked = true;
                    Boxes[0].SetActive(true);
                    Destroy(other.gameObject);
                    BackBox = false;
                    TakingBox = false;
                }
                if (other.name == "A - Copy(Clone)")
                {
                    BoxAreTaked = true;
                    Boxes[1].SetActive(true);
                    Destroy(other.gameObject);
                    BackBox = false;
                    TakingBox = false;
                }
                if (other.name == "B - Copy(Clone)")
                {
                    BoxAreTaked = true;
                    Boxes[2].SetActive(true);
                    Destroy(other.gameObject);
                    BackBox = false;
                    TakingBox = false;
                }
                if (other.name == "I - Copy(Clone)")
                {
                    BoxAreTaked = true;
                    Boxes[3].SetActive(true);
                    Destroy(other.gameObject);
                    BackBox = false;
                    TakingBox = false;
                }
                if (other.name == "Q - Copy(Clone)")
                {
                    BoxAreTaked = true;
                    Boxes[4].SetActive(true);
                    Destroy(other.gameObject);
                    BackBox = false;
                    TakingBox = false;
                }
                if (other.name == "R - Copy(Clone)")
                {
                    BoxAreTaked = true;
                    Boxes[5].SetActive(true);
                    Destroy(other.gameObject);
                    BackBox = false;
                    TakingBox = false;
                }
                if (other.name == "W - Copy(Clone)")
                {
                    BoxAreTaked = true;
                    Boxes[6].SetActive(true);
                    Destroy(other.gameObject);
                    BackBox = false;
                    TakingBox = false;
                }
            }
        }
        if (other.CompareTag("dropCubes") && BoolM.LevelOne == true)
        {
            foreach (GameObject AA in Boxes)
            {
                if (AA.activeSelf == true)
                {
                    foreach (GameObject AABox in other.GetComponent<CheckManagerPositionCubes>().ListPositionBoxes)
                    {
                        if (AABox.activeSelf == true)
                        {
                            Instantiate(AA, new Vector3(AABox.transform.position.x, AABox.transform.position.y + 1f, AABox.transform.position.z), AABox.transform.rotation);
                            if (FixingSpawningBox == true)
                            {
                                other.GetComponent<CheckManagerPositionCubes>().ActivateBoxs();
                                FixingSpawningBox = false;
                            }
                        }
                    }
                    Launcher.CurrentActiveBox += 1;
                    Launcher.ManagerBoxesActivation();
                    BackBox = true;
                    TakingBox = true;
                    AA.SetActive(false);
                }
            }
        }
        if(other.CompareTag("Food") && BoolM.LevelTwo == true)
        {
            if(FixingTakingFood == true)
            {
                foreach (GameObject AA in Foods)
                {
                    if (other.name == AA.name + "(Clone)")
                    {
                        AA.SetActive(true);
                    }
                }
                Destroy(other.gameObject);
                BackToStation = true;
                FixingTakingFood = false;
            }
        }
        if (other.CompareTag("DropFood") && BoolM.LevelTwo == true)
        {
            if(FixingTakingFood == false)
            {
                foreach (GameObject AA in Foods)
                {
                    if (AA.activeSelf == true)
                    {
                        if (AA.name == "foodbagBlue" && AA.activeSelf == true)
                        {
                            ManagerFlexing.GetComponent<voistartController>().ValueBlue += 0.05f;
                            ManagerFlexing.GetComponent<voistartController>().Blue.StartPlayback();
                        }
                        if (AA.name == "foodbagGreen" && AA.activeSelf == true)
                        {
                            ManagerFlexing.GetComponent<voistartController>().ValueYellow += 0.05f;
                            ManagerFlexing.GetComponent<voistartController>().Yellow.StartPlayback();
                        }
                        if (AA.name == "foodbagRed" && AA.activeSelf == true)
                        {
                            ManagerFlexing.GetComponent<voistartController>().ValueRed += 0.05f;
                            ManagerFlexing.GetComponent<voistartController>().Red.StartPlayback();
                        }
                        AA.SetActive(false);
                    }
                }
                BackToStation = false;
                FixingTakingFood = true;
            }
        }
        if (other.CompareTag("Leds") && BoolM.LevelThree == true)
        {
            if (FixingLeds == true)
            {
                BackTostationLeds = true;
                foreach (GameObject AA in Leds)
                {
                    if (AA.name + "(Clone)" == other.name)
                    {
                        AA.SetActive(true);
                        Destroy(other.gameObject);
                    }
                }
                FixingLeds = false;
            }
        }
        if (other.CompareTag("LedsDrop") && BoolM.LevelThree == true)
        {
            if (FixingLeds == false)
            {
                BackTostationLeds = false;
                foreach (GameObject AA in Leds)
                {
                    foreach (GameObject AA2 in Launcher.ManagerLight.LedsObjs)
                    {
                        if (AA2.name == AA.name && AA.activeSelf == true)
                        {
                            (Instantiate(AA2, Launcher.ManagerLight.ListPositionLeds[Launcher.ManagerLight.CurrentLedActive].transform.position, AA2.transform.rotation) as GameObject).transform.SetParent(Launcher.ManagerLight.ListPositionLeds[Launcher.ManagerLight.CurrentLedActive].transform);
                            Launcher.ManagerLight.CurrentLedActive += 1;
                        }
                    }
                    AA.SetActive(false);
                }
                FixingLeds = true;
            }
        }
        if (other.CompareTag("Battrey") && BoolM.LevelFour == true)
        {
            if (FixingBattrey == true)
            {
                BattreyFollow = true;
                BacktoStationBattrey = true;
                CurrentBattrey.SetActive(true);
                Destroy(other.gameObject);
                FixingBattrey = false;
            }
        }
        if (other.CompareTag("BattreyDrop") && BoolM.LevelFour == true)
        {
            if (FixingBattrey == false)
            {
                BacktoStationBattrey = false;
                other.gameObject.GetComponent<TriggerCheckerBattery>().CheckRedness.CheckCurrentPile();
                CurrentBattrey.SetActive(false);
                FixingBattrey = true;
            }
        }
        if (other.CompareTag("Lamp") && BoolM.LevelFive == true == true)
        {
            if (SpawningFixingLamps == true)
            {
                foreach (GameObject Lamping in CurrentLamps)
                {
                    BackToLamps = true;
                    Lamping.SetActive(false);
                    if (other.name == Lamping.name + "(Clone)")
                    {
                        Lamping.SetActive(true);
                    }
                    Destroy(other.gameObject);
                }
                SpawningFixingLamps = false;
            }
        }
        if (other.CompareTag("LampDrop") && BoolM.LevelFive == true == true)
        {
            if (SpawningFixingLamps == false)
            {
                foreach (GameObject Lamping in CurrentLamps)
                {
                    BackToLamps = false;
                    Lamping.SetActive(false);
                }
                other.GetComponent<DetecteurSanta>().ManagerP.ActivateLed();
                other.GetComponent<DetecteurSanta>().ManagerP.currentled += 1;
                SpawningFixingLamps = true;
            }
        }
    }
}
