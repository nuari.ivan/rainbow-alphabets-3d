using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;
using TMPro;

public class ManagerLeaderBoard : MonoBehaviour
{
    public TextMeshProUGUI[] playerName;
    public GameObject DeadIcon;
    public TextMeshProUGUI DeadMessage;
    public GameObject Container;

    internal bool CheckOneTime = true;

    private void Start()
    {
        
    }
    private void Update()
    {
        if (playerName[1].gameObject.activeSelf == true && CheckOneTime == true)
        {
            StartCoroutine(BackAnimationBack());
            DeadMessage.gameObject.SetActive(true);
            DeadMessage.text = "<color=#FE3C3C>" + playerName[0].text + "</color> was found!";
            (Instantiate(DeadMessage.gameObject, DeadMessage.gameObject.transform.position, DeadMessage.gameObject.transform.rotation) as GameObject).transform.SetParent(Container.gameObject.transform);
            DeadMessage.gameObject.SetActive(false);
            CheckOneTime = false;
        }   
    }IEnumerator BackAnimationBack()
    {
        yield return new WaitForSeconds(1.8f);
        DeadMessage.gameObject.SetActive(false);
    }
}
