using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class powerUpTime : MonoBehaviour
{
    public Timer TimerManager;
    public void AddTimer()
    {
        TimerManager.timeRemaining += 30;
    }

    public void WatchAds(string tipe)
    {
        AdsManager.Instance.RequestRewardedAds(tipe);
    }

    private void OnEnable()
    {
        AdsManager.Instance.OnAdsRewarded += Instance_OnAdsRewarded;
    }

    private void OnDisable()
    {
        AdsManager.Instance.OnAdsRewarded -= Instance_OnAdsRewarded;
    }
    private void Instance_OnAdsRewarded(string placement)
    {
        if (placement == "time")
        {
            AddTimer();
        }
        if (placement == "boxes")
        {

        }
        if (placement == "speed")
        {

        }
    }
}
