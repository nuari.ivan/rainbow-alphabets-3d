using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class destroySelf : MonoBehaviour
{
    public float TimeDestroying = 0;
    private TextMeshProUGUI CurrentText;
    [Header("Nmaes Strings")]
    public string[] last = new string[] { "avon", "bage", "beck", "borne", "borough", "bourne", "bridge", "brook", "brough", "bury", "by", "castle",
            "cester", "chester", "combe", "den", "ditch", "don", "down", "ey", "field", "ford", "grove", "hall", "ham", "hampton", "head", "lake",
            "ley", "ling", "low", "mere", "moor", "nell", "ney", "over", "port", "shot", "side", "smith", "sted", "stoke", "thorne", "ton", "tree",
            "wang", "well", "wich", "wick", "wold", "wood", "worth" };

    void Start()
    {
        StartCoroutine(CheckingGoodBy());
        CurrentText = GetComponent<TextMeshProUGUI>();
        CurrentText.text = "<color=#FE3C3C>" + last[Random.Range(0, last.Length)] + "</color> Its Joind!";
    }
    IEnumerator CheckingGoodBy()
    {
        yield return new WaitForSeconds(TimeDestroying);
        Destroy(this.gameObject);
    }
}
