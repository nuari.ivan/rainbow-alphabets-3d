using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraManager : MonoBehaviour
{
    [Header("Controlling Players")]
    public BooleanManager BoolM;
    public GameObject Player;

    [Header("Cameras")]
    public GameObject NormalCamera;
    public GameObject NightCamera;

    [Header("Vectors")]
    public Vector3 Offest;
    private Vector3 Rb = Vector3.zero;

    public float SmoothFollow = 0.2f;

    [Header("floating Manager")]
    internal float ValeurY = -13;
    internal float Valeurz = 10;

    [Header("Boolean manager")]
    internal bool CameraBack = true;

    void Update()
    {
        if(BoolM.LevelFour == true)
        {
            NightCamera.SetActive(true);
            NormalCamera.SetActive(false);
        }
        else
        {
            NightCamera.SetActive(false);
            NormalCamera.SetActive(true);
        }
    }
    void FixedUpdate()
    {
        if(Player != null)
        {
            transform.position = Vector3.SmoothDamp(transform.position, Player.transform.position + Offest, ref Rb, SmoothFollow);
            transform.rotation = Quaternion.identity;
        }
    }
}
