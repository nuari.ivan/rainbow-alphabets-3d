using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MonsterGreenShadow : MonoBehaviour
{
    public GameLauncherController Launcher;
    public GameObject CameraMain;
    public GameObject GameUI;

    public GameObject[] AllMonsters;
    internal int CurrentActiveMonster = 0;

    void Start()
    {
        Launcher = GameObject.Find("GameLauncher").GetComponent<GameLauncherController>();
        CameraMain = GameObject.FindGameObjectWithTag("MainCamera");
        GameUI = GameObject.Find("GameUI");
        StartCoroutine(CheckingActiveMonster());
    }
    IEnumerator CheckingActiveMonster()
    {
        yield return new WaitForSeconds(0.5f);
        if (Launcher.GameOver == false)
        {
            CurrentActiveMonster = Random.Range(0, AllMonsters.Length);
            AllMonsters[CurrentActiveMonster].gameObject.SetActive(true);
            yield return new WaitForSeconds(5f);
            foreach (GameObject monster in AllMonsters)
            {
                monster.SetActive(false);
            }
            CurrentActiveMonster = Random.Range(0, AllMonsters.Length);
            AllMonsters[CurrentActiveMonster].gameObject.SetActive(true);
            StartCoroutine(CheckingActiveMonster());
        }
    }
}
