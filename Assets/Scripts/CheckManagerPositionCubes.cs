using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CheckManagerPositionCubes : MonoBehaviour
{
    public GameObject[] ListPositionBoxes;
    public BooleanManager BoolM;
    internal int CurrentActiveBox = 0;

    void Start()
    {
        if(BoolM.LevelOne == true)
        {
            ListPositionBoxes[0].SetActive(true);
        }
    }
    public void ActivateBoxs()
    {
        foreach(GameObject AA in ListPositionBoxes)
        {
            AA.SetActive(false);
        }
        ListPositionBoxes[CurrentActiveBox].SetActive(true);
        CurrentActiveBox += 1;
    }
}
