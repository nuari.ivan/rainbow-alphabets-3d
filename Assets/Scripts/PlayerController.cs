using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;
using TMPro;
using System.Linq;
using UnityEditor;
using UnityEngine.UIElements;

public class PlayerController : MonoBehaviour
{
    [Header("Manager")]
    public joystickManager movementJoystick;
    public GameLauncherController ControllerLancher;
    public ManagerLeaderBoard LeaderController;
    public BooleanManager BoolM;
    public voistartController ControllerFoods;
    public voidStartFoal ManagerLight;

    [Header("Floating Manager")]
    internal float PlayerSpeed;
    internal float AddSpeed = 0;

    [Header("Name Player")]
    public TextMeshProUGUI NamePlayer;
    public TextMeshProUGUI CurrentName;

    [Header("Night Mode")]
    public GameObject FLash;
    public GameObject CurrentBattrey;

    [Header("RigidBody managers")]
    private Rigidbody rb;

    [Header("All Cameras New")]
    public GameObject[] CamerasDead;

    [Header("Audios Manager")]
    public AudioSource SRC;
    public AudioClip footSteps;
    public AudioClip LaughingSND;

    [Header("Character Controller")]
    public GameObject[] AllCharacters;

    [Header("All Objects")]
    public GameObject[] Boxes;
    public GameObject[] Foods;
    public GameObject[] Leds;
    public GameObject[] Lamps;

    [Header("Animation Controlling")]
    public Animator[] AllAnimation;
    public Animator BodyAnimation;

    [Header("integer Manager")]
    internal int TakeMoreBox = 0;

    [Header("Boolean manager")]
    internal bool BoxRunner = false;
    internal bool CharacterRunner = true;
    internal bool PlayerMoving = true;
    internal bool StepsMoving = true;
    internal bool TakingBox = true;
    internal bool TakinFood = true;
    internal bool FixingSpawningBox = true;
    internal bool FixingLeds = true;
    internal bool FixingBattrey = true;
    internal bool SpawningFixingLamps = true;

    void Start()
    {
        rb = GetComponent<Rigidbody>();
        ManagerInfoPlayer();
        CurrentName.text = PlayerPrefs.GetString("PlayerName");
        StartCoroutine(LaughingController());
    }
    void Update()
    {
        if(BoxRunner == true)
        {
            PlayerSpeed = 4 + AddSpeed;
        }
        else
        {
            PlayerSpeed = 7.5f + AddSpeed;
        }
        if(BoolM.LevelFour == true)
        {
            FLash.SetActive(true);
        }
        else if(BoolM.LevelFour == false)
        {
            FLash.SetActive(false);
        }
    }
    void FixedUpdate()
    {
        if (movementJoystick.joystickVec.y != 0 && PlayerMoving == true)
        {
            rb.velocity = new Vector3(movementJoystick.joystickVec.x * PlayerSpeed, rb.velocity.y, movementJoystick.joystickVec.y * PlayerSpeed);
            transform.eulerAngles = new Vector3(0, Mathf.Atan2(movementJoystick.joystickVec.x, movementJoystick.joystickVec.y) * 180 / Mathf.PI, 0);
            if(StepsMoving == true)
            {
                SRC.clip = footSteps;
                StartCoroutine(StepsController());
                StepsMoving = false;
            }
            ManagerMovingPlayerAnimation();
        }
        else
        {
            rb.velocity = Vector3.zero;
            ManagerAnimationIdling();
            if(StepsMoving == false)
            {
                StopAllCoroutines();
                StepsMoving = true;
            }
        }
    }
    void ManagerAnimationIdling()
    {
        foreach (Animator AAPlayer in AllAnimation)
        {
            if (AAPlayer.gameObject.activeSelf == true && BoxRunner == true)
            {
                AAPlayer.Play("idle_box");
            }
            if (AAPlayer.gameObject.activeSelf == true && CharacterRunner == true)
            {
                AAPlayer.Play("idle");
            }
        }
    }
    void ManagerMovingPlayerAnimation()
    {
        foreach (Animator AAPlayer in AllAnimation)
        {
            if (AAPlayer.gameObject.activeSelf == true && BoxRunner == true)
            {
                AAPlayer.Play("run_box");
            }
            if (AAPlayer.gameObject.activeSelf == true && CharacterRunner == true)
            {
                AAPlayer.Play("run");
            }
        }
    }
    void ManagerInfoPlayer()
    {
        NamePlayer.text = PlayerPrefs.GetString("PlayerName") +"(<color=#58EAFF>x2</color>)" ;
        foreach (GameObject Character in AllCharacters)
        {
            if (PlayerPrefs.GetString("CurrentCharacter") == Character.name)
            {
                Character.SetActive(true);
            }
        }
    }
    IEnumerator LaughingController()
    {
        yield return new WaitForSeconds(8f);
        SRC.clip = LaughingSND;
        SRC.Play();
        StartCoroutine(LaughingController());
    }
    IEnumerator StepsController()
    {
        yield return new WaitForSeconds(0.3f);
        if (movementJoystick.joystickVec.y != 0)
        {
            SRC.Play();
        }
        StartCoroutine(StepsController());
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Box") && BoolM.LevelOne == true)
        {
            if(TakingBox == true)
            {
                if (other.name == "X - Copy(Clone)")
                {
                    ControllerLancher.TotalBoxTakeit += 1;
                    Boxes[0].SetActive(true);
                    Destroy(other.gameObject);
                    TakingBox = false;
                }
                if (other.name == "A - Copy(Clone)")
                {
                    ControllerLancher.TotalBoxTakeit += 1;
                    Boxes[1].SetActive(true);
                    Destroy(other.gameObject);
                    TakingBox = false;
                }
                if (other.name == "B - Copy(Clone)")
                {
                    ControllerLancher.TotalBoxTakeit += 1;
                    Boxes[2].SetActive(true);
                    Destroy(other.gameObject);
                    TakingBox = false;
                }
                if (other.name == "I - Copy(Clone)")
                {
                    ControllerLancher.TotalBoxTakeit += 1;
                    Boxes[3].SetActive(true);
                    Destroy(other.gameObject);
                    TakingBox = false;
                }
                if (other.name == "Q - Copy(Clone)")
                {
                    ControllerLancher.TotalBoxTakeit += 1;
                    Boxes[4].SetActive(true);
                    Destroy(other.gameObject);
                    TakingBox = false;
                }
                if (other.name == "R - Copy(Clone)")
                {
                    ControllerLancher.TotalBoxTakeit += 1;
                    Boxes[5].SetActive(true);
                    Destroy(other.gameObject);
                    TakingBox = false;
                }
                if (other.name == "W - Copy(Clone)")
                {
                    ControllerLancher.TotalBoxTakeit += 1;
                    Boxes[6].SetActive(true);
                    Destroy(other.gameObject);
                    TakingBox = false;
                }
                if(TakeMoreBox == 1)
                {
                    TakingBox = true;
                }
            }
        }
        if (other.CompareTag("dropCubes") && BoolM.LevelOne == true)
        {
            foreach(GameObject AA in Boxes)
            {
                if(AA.activeSelf == true)
                {
                    foreach(GameObject AABox in other.GetComponent<CheckManagerPositionCubes>().ListPositionBoxes)
                    {
                        if(AABox.activeSelf == true)
                        {
                            Instantiate(AA, new Vector3(AABox.transform.position.x, AABox.transform.position.y + 1f, AABox.transform.position.z), AABox.transform.rotation);
                            if(FixingSpawningBox == true)
                            {
                                other.GetComponent<CheckManagerPositionCubes>().ActivateBoxs();
                                FixingSpawningBox = false;
                            }
                        }
                    }
                    ControllerLancher.CurrentActiveBox += 1;
                    ControllerLancher.ManagerBoxesActivation();
                    TakingBox = true;
                    AA.SetActive(false);
                }
            }
        }
        if(other.CompareTag("Food") && BoolM.LevelTwo == true)
        {
            foreach (GameObject AA in Foods)
            {
                if(AA.name + "(Clone)" == other.name && TakinFood == true)
                {
                    AA.SetActive(true);
                    Destroy(other.gameObject);
                    TakinFood = false;
                }
            }
            if(TakeMoreBox == 1)
            {
                TakinFood = true;
                TakeMoreBox = 0;
            }
        }
        if (other.CompareTag("DropFood") && BoolM.LevelTwo == true)
        {
            if(TakinFood == false)
            {
                foreach (GameObject AA in Foods)
                {
                    if (AA.name == "foodbagBlue" && AA.activeSelf == true)
                    {
                        ControllerFoods.ValueBlue += 0.05f;
                        ControllerFoods.Blue.StartPlayback();
                    }
                    if (AA.name == "foodbagGreen" && AA.activeSelf == true)
                    {
                        ControllerFoods.ValueYellow += 0.05f;
                        ControllerFoods.Yellow.StartPlayback();
                    }
                    if (AA.name == "foodbagRed" && AA.activeSelf == true)
                    {
                        ControllerFoods.ValueRed += 0.05f;
                        ControllerFoods.Red.StartPlayback();
                    }
                    AA.SetActive(false);
                }
                TakinFood = true;
            }
        }
        if(other.CompareTag("Leds") && BoolM.LevelThree == true)
        {
            if(FixingLeds == true)
            {
                foreach (GameObject AA in Leds)
                {
                    if (AA.name + "(Clone)" == other.name)
                    {
                        AA.SetActive(true);
                        Destroy(other.gameObject);
                    }
                }
                FixingLeds = false;
            }
            if(TakeMoreBox == 1)
            {
                FixingLeds = true;
            }
        }
        if (other.CompareTag("LedsDrop") && BoolM.LevelThree == true)
        {
            if(FixingLeds == false)
            {
                foreach (GameObject AA in Leds)
                {
                    foreach(GameObject AA2 in ManagerLight.LedsObjs)
                    {
                        if(AA2.name == AA.name && AA.activeSelf == true)
                        {
                            (Instantiate(AA2, ManagerLight.ListPositionLeds[ManagerLight.CurrentLedActive].transform.position, AA2.transform.rotation) as GameObject).transform.SetParent(ManagerLight.ListPositionLeds[ManagerLight.CurrentLedActive].transform);
                            ManagerLight.CurrentLedActive += 1;
                        }
                    }
                    AA.SetActive(false);
                }
                FixingLeds = true;
            }
        }
        if(other.CompareTag("Battrey") && BoolM.LevelFour == true)
        {
            if(FixingBattrey == true)
            {
                CurrentBattrey.SetActive(true);
                Destroy(other.gameObject);
                FixingBattrey = false;
            }
        }
        if (other.CompareTag("BattreyDrop") && BoolM.LevelFour == true)
        {
            if(FixingBattrey == false)
            {
                other.gameObject.GetComponent<TriggerCheckerBattery>().CheckRedness.CheckCurrentPile();
                CurrentBattrey.SetActive(false);
                FixingBattrey = true;
            }
        }
        if(other.CompareTag("Lamp") && BoolM.LevelFive== true == true)
        {
            if(SpawningFixingLamps == true)
            {
                foreach (GameObject Lamping in Lamps)
                {
                    Lamping.SetActive(false);
                    if (other.name == Lamping.name + "(Clone)")
                    {
                        Lamping.SetActive(true);
                    }
                    Destroy(other.gameObject);
                }
                SpawningFixingLamps = false;
            }
        }
        if (other.CompareTag("LampDrop") && BoolM.LevelFive == true == true)
        {
            if(SpawningFixingLamps == false)
            {
                foreach (GameObject Lamping in Lamps)
                {
                    Lamping.SetActive(false);
                }
                other.GetComponent<DetecteurSanta>().ManagerP.ActivateLed();
                other.GetComponent<DetecteurSanta>().ManagerP.currentled += 1;
                SpawningFixingLamps = true;
            }
        }
        if (other.CompareTag("CharacterFollow"))
        {
            Debug.Log(other.name);
        }
    }
}
