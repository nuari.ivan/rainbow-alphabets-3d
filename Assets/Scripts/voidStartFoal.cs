using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class voidStartFoal : MonoBehaviour
{
    public GameObject Effects;
    public GameObject MonsterRed;
    public Transform PositionRed;
    internal int CurrentLedActive = 0;
    public GameObject[] ListPositionLeds;
    public GameObject[] LedsObjs;

    void Start()
    {
        MonsterRed.transform.position = PositionRed.transform.position;
    }
    private void Update()
    {
        if(CurrentLedActive >= 12)
        {
            Effects.SetActive(true);
        }
    }
}
