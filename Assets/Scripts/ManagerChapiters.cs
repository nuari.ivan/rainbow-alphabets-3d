using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityEngine;

public class ManagerChapiters : MonoBehaviour
{
    [Header("Componenet Manager")]
    public GameObject[] UnlockedPart;
    public GameObject LockedPart;

    [Header("Loading Scene")]
    public GameObject LoadingScreen;

    [Header("Integer Manager")]
    internal int CurrentSceneNumber;

    [Header("Strings Manager")]
    public string CurrentTag;

    void Start()
    {
        if(PlayerPrefs.GetString(CurrentTag) == "Done")
        {
            UnlockedPart[0].SetActive(true);
            LockedPart.SetActive(false);
        }
        else
        {
            LockedPart.SetActive(true);
            UnlockedPart[0].SetActive(false);
        }
    }
    public void LoadScene(int CurrentScene)
    {
        StartCoroutine(LoadingTime());
        CurrentSceneNumber = CurrentScene;
    }
    IEnumerator LoadingTime()
    {
        LoadingScreen.SetActive(true);
        yield return new WaitForSeconds(2f);
        SceneManager.LoadScene(CurrentSceneNumber);
    }
}
