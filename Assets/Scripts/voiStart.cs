using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityEngine;
using TMPro;

public class voiStart : MonoBehaviour
{
    [Header("Name Manager")]
    public TextMeshProUGUI CurrentName;

    [Header("Current Cash Add")]
    internal int CurrentCash = 0;

    [Header("Boolean Manager")]
    internal bool CheckCompleteLevel = true;

    private void OnEnable()
    {
        AdsManager.Instance.OnAdsRewarded += Instance_OnAdsRewarded; 
    }

    private void OnDisable()
    {
        AdsManager.Instance.OnAdsRewarded -= Instance_OnAdsRewarded;
    }

    private void Instance_OnAdsRewarded(string placement)
    {
        // saat reward ads diberikan selesai apa yang akan didapat player tulis kodenya dibawah ini!
    }

    void Start()
    {
        ManagerStart();
        if (PlayerPrefs.GetString("CurrentLevelOne") == "" && CheckCompleteLevel == true)
        {
            PlayerPrefs.SetString("CurrentLevelOne", "Done");
            Debug.Log("Level One Completed");
            CheckCompleteLevel = false;
        }
        else if(CheckCompleteLevel == true)
        {
            if (PlayerPrefs.GetString("CurrentLevelTwo") == "")
            {
                PlayerPrefs.SetString("CurrentLevelTwo", "Done");
                Debug.Log("Level Two Completed");
                CheckCompleteLevel = false;
            }
            else if(CheckCompleteLevel == true)
            {
                if (PlayerPrefs.GetString("CurrentLevelThree") == "")
                {
                    PlayerPrefs.SetString("CurrentLevelThree", "Done");
                    Debug.Log("Level Three Completed");
                    CheckCompleteLevel = false;
                }
                else if (CheckCompleteLevel == true)
                {
                    if (PlayerPrefs.GetString("CurrentLevelFour") == "")
                    {
                        PlayerPrefs.SetString("CurrentLevelFour", "Done");
                        Debug.Log("Level Four Completed");
                        CheckCompleteLevel = false;
                    }
                    else if (CheckCompleteLevel == true)
                    {
                        if (PlayerPrefs.GetString("CurrentLevelFive") == "")
                        {
                            PlayerPrefs.SetString("CurrentLevelFive", "Done");
                            Debug.Log("Level Five Completed");
                            CheckCompleteLevel = false;
                        }
                        else if (CheckCompleteLevel == true)
                        {
                            if (PlayerPrefs.GetString("CurrentLevelSix") == "")
                            {
                                PlayerPrefs.SetString("CurrentLevelSix", "Done");
                                Debug.Log("Level Six Completed");
                                CheckCompleteLevel = false;
                            }
                        }
                    }
                }
            }
        }
    }
    void ManagerStart()
    {
        CurrentName.text = PlayerPrefs.GetString("CurrentName");
    }
    public void ExitWinPopu()
    {
        AdsManager.Instance.RequestInterstitial();
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex - 1);
    }
    public void NextCoin()
    {
        AdsManager.Instance.RequestRewardedAds("AdsResult");
        PlayerPrefs.SetInt("CurrentMoney", CurrentCash);
    }
}
