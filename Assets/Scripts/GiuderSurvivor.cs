using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityEngine.AI;
using TMPro;

public class GiuderSurvivor : MonoBehaviour
{
    public GameLauncherController GameManager;
    public BooleanManager BoolM;
    private NavMeshAgent NVmesh;
    private Rigidbody rb;
    private int CurrentLocalisation = 0;
    public GameObject[] PositionsFollow;
    public Animator AnimationAnim;
    public GameObject UIObject;
    public GameObject Player;
    public GameObject[] NarratorDirection;

    void Start()
    {
        Player = GameObject.FindGameObjectWithTag("Player");
        NVmesh = GetComponent<NavMeshAgent>();rb = GetComponent<Rigidbody>();
        CurrentLocalisation = Random.Range(0, PositionsFollow.Length);
        StartCoroutine(CheckingUIPOP());
        ManagerCurrentLevel();
    }
    void Update()
    {
        PositionsFollow = GameObject.FindGameObjectsWithTag("CharacterFollow");
        if(PositionsFollow.Length > 0 && GameManager.GameStart == true)
        {
            NVmesh.SetDestination(PositionsFollow[CurrentLocalisation].transform.position);
            if (transform.position.x == PositionsFollow[CurrentLocalisation].transform.position.x && transform.position.y == PositionsFollow[CurrentLocalisation].transform.position.y)
            {
                CurrentLocalisation += 1;
            }
        }
        if(PositionsFollow == null)
        {
            NVmesh.SetDestination(Player.transform.position);
        }
    }
    void FixedUpdate()
    {
        if(rb.velocity.y != 0 && GameManager.GameStart == true)
        {
            AnimationAnim.Play("walk");
        }else
        {
            AnimationAnim.Play("idle");
        }
    }
    void ManagerCurrentLevel()
    {
        if(BoolM.LevelOne == true)
        {
            NarratorDirection[0].SetActive(true);
        }
        if(BoolM.LevelTwo == true)
        {
            NarratorDirection[1].SetActive(true);
        }
        if(BoolM.LevelThree == true)
        {
            NarratorDirection[2].SetActive(true);
        }
        if(BoolM.LevelFour == true)
        {
            NarratorDirection[3].SetActive(true);
        }
        if(BoolM.LevelFive == true)
        {
            NarratorDirection[4].SetActive(true);
        }
        if(BoolM.LevelSix == true)
        {
            NarratorDirection[5].SetActive(true);
        }
    }
    IEnumerator CheckingUIPOP()
    {
        yield return new WaitForSeconds(5);
        UIObject.transform.localScale = new Vector3();
    }
}
