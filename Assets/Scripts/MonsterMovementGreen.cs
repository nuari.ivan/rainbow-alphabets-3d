using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.TextCore.Text;

public class MonsterMovementGreen : MonoBehaviour
{
    [Header("Manager Controller")]
    public MonsterGreenShadow ControllerMonster;

    [Header(" Cameras Manager ")]
    public GameObject CameraKilled;
    public GameObject Player;

    [Header("Animation Manager")]
    public Animator MonsterAnimation;
    public Animator[] Characters;

    [Header("Boolean Manager")]
    internal bool PlayerCatched = false;
    internal bool EnemyCatched = false;
    internal bool FixingCatched = true;

    void Awake()
    {
        Player = GameObject.FindGameObjectWithTag("Player");
    }
    void Start()
    {

    }
    void Update()
    {
        if(FixingCatched == true && PlayerCatched == true)
        {
            foreach (GameObject CurrentCharacter in Player.GetComponent<PlayerController>().AllCharacters)
            {
                if(CurrentCharacter.activeSelf == true)
                {
                    foreach (Animator AA2 in Characters)
                    {
                        if(CurrentCharacter.name == AA2.name)
                        {
                            AA2.gameObject.SetActive(true);
                            ControllerMonster.GameUI.GetComponent<GameUI>().NightUI.SetActive(false);
                            ControllerMonster.Launcher.GameOver = true;
                            ControllerMonster.CameraMain.gameObject.SetActive(false);
                            AA2.Play("drag");
                            MonsterAnimation.Play("attack");
                        }

                    }
                }
            }
            CameraKilled.SetActive(true);
            FixingCatched = false;
        }
    }
}
