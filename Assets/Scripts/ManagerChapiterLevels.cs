using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ManagerChapiterLevels : MonoBehaviour
{
    public GameObject ManagerLocation;
    void Update()
    {

    }
    void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            ManagerLocation.SetActive(true);
            ManagerLocation.GetComponent<Animator>().Play("PopUp");
        }
    }
    void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            ManagerLocation.GetComponent<Animator>().Play("PopUpClose");
        }
    }
}
