using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShopSkinsManager : MonoBehaviour
{
    [Header("Integer Manager")]
    internal int CurrentCharacter = 0;

    [Header("Compoenet Controlle Current")]
    public GameObject[] AllotherObjects;

    private void OnEnable()
    {
        AdsManager.Instance.OnAdsRewarded += Instance_OnAdsRewarded;
    }

    private void OnDisable()
    {
        AdsManager.Instance.OnAdsRewarded -= Instance_OnAdsRewarded;
    }

    private void Instance_OnAdsRewarded(string placement)
    {
        if(placement == "Get500")
        {
            int money = PlayerPrefs.GetInt("CurrentMoney", 0);
            money += 500;
            PlayerPrefs.SetInt("CurrentMoney", money);
        }
    }

    public void ManagerCurrentCharacter()
    {
        foreach(GameObject AA in AllotherObjects)
        {
            AA.SetActive(false);
        }
        AllotherObjects[CurrentCharacter].SetActive(true);
    }
    public void WatchAdReward()
    {
        AdsManager.Instance.RequestRewardedAds("Get500");
    }
    public void ChaseRandoom()
    {
        int money = PlayerPrefs.GetInt("CurrentMoney", 0);
        money += Random.Range(100, 2000);
        PlayerPrefs.SetInt("CurrentMoney", money);
    }
}
