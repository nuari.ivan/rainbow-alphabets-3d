using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;
using TMPro;

public class LeaderBoardManager : MonoBehaviour
{
    [Header("Panels Manager")]
    public GameObject PanelName;

    [Header("Manager ui")]
    public TMP_InputField InputField;
    public TextMeshProUGUI[] NameList;

    [Header("Images Buttons Controller")]
    public Image BtnWorldWide;
    public Image BtnCountry;

    [Header("Sprites Controller")]
    public Sprite Active;
    public Sprite Inactive;

    void Start()
    {
        if(PlayerPrefs.GetString("NameLeaderBoard") == "")
        {
            PanelName.SetActive(true);
        }
        else
        {
            PanelName.SetActive(false);
        }
    }
    void Update()
    {
        foreach(TextMeshProUGUI AA in NameList)
        {
            AA.text = PlayerPrefs.GetString("NameLeaderBoard");
        }
    }
    public void Country()
    {
        BtnCountry.sprite = Active;
        BtnWorldWide.sprite = Inactive;
    }
    public void WorldWide()
    {
        BtnWorldWide.sprite = Active;
        BtnCountry.sprite = Inactive;
    }
    public void ManagerController()
    {
        string CurrentName = InputField.text;
        PlayerPrefs.SetString("NameLeaderBoard", CurrentName);
        PanelName.SetActive(false);
    }
}
