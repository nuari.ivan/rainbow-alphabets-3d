using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityEngine;
using TMPro;

public class HomeLauncher : MonoBehaviour
{
    [Header("Controlling Background")]
    public RawImage BackgroundBg;

    [Header("Controlling Panels")]
    public GameObject PanelName;
    public GameObject PanelSelectSkin;
    public GameObject PanelHome;
    public GameObject PanelIAP;
    public GameObject PanelPause;
    public GameObject RemoveAds;
    public GameObject LeaderBoard;
    public GameObject DialyReward;
    public GameObject ShopSkins;

    [Header("Loading Screen")]
    public GameObject LoadingScene;

    [Header("Controlling NamePlayer")]
    public TMP_InputField InputField;
    public TextMeshProUGUI[] AllNames;
    public TMP_Text labelMoney;

    [Header("Floating Manager")]
    internal float bgValeurX;
    internal float bgValeurY;

    [Header("Boolean Manager")]
    internal bool OpenIAP = false;
    internal bool OpenSetting = false;
    internal bool OpenRemoveAds = true;
    internal bool OpenLeaderBoard = true;
    internal bool OpenDialyReward = true;
    internal bool OpenShopSkins = true;

    private void OnEnable()
    {
        int money = PlayerPrefs.GetInt("CurrentMoney", 15000);
        if(money>=1000)
        {
            labelMoney.text = $"{money/1000}K";
        }
        
    }

    void Start()
    {
        ControllerGame();
        Time.timeScale = 1.0f;
        if(PlayerPrefs.GetString("CurrentCharacter") != "")
        {
            ManagerGamePlay();
        }
    }
    void Update()
    {
        ManagerBg();
        ControllingNames();
    }
    void ControllerGame()
    {
        if(PlayerPrefs.GetString("PlayerName") == "")
        {
            PanelName.SetActive(true);
            PanelHome.SetActive(false);
        }
        else
        {
            PanelName.SetActive(false);
            if (PlayerPrefs.GetString("CurrentCharacter") == "")
            {
                PanelSelectSkin.SetActive(true);
            }
            else
            {
                PanelSelectSkin.SetActive(false);
                PanelHome.SetActive(true);
            }
        }
    }
    public void ManagerGamePlay()
    {
        LoadingScene.SetActive(true);
        StartCoroutine(CheckingLoadingScreen());
    }
    void ManagerBg()
    {
        bgValeurX -= Time.deltaTime / 8;
        bgValeurY -= Time.deltaTime / 8;
        BackgroundBg.uvRect = new Rect(bgValeurX, bgValeurY, 1, 10);
    }
    void ControllingNames()
    {
        foreach(TextMeshProUGUI AA in AllNames)
        {
            AA.text = PlayerPrefs.GetString("PlayerName");
        }
    }
    public void LevelNext()
    {
        StartCoroutine(LoadingLevel());
    }
    public void OpenShopSkinsPanel()
    {
        if(OpenShopSkins == true)
        {
            ShopSkins.SetActive(true);
        }
        else
        {
            if(OpenShopSkins == false)
            {
                ShopSkins.SetActive(false);
            }
        }
        OpenShopSkins = !OpenShopSkins;
    }
    public void OpenDialyRewardPanel()
    {
        if(OpenDialyReward == true)
        {
            DialyReward.SetActive(true);
        }
        else
        {
            if(OpenDialyReward == false)
            {
                DialyReward.SetActive(false);
            }
        }
        OpenDialyReward = !OpenDialyReward;
    }
    public void OpenLeaderBoardPanel()
    {
        if(OpenLeaderBoard == true)
        {
            LeaderBoard.SetActive(true);
        }
        else
        {
            if(OpenLeaderBoard == false)
            {
                LeaderBoard.SetActive(false);
            }
        }
        OpenLeaderBoard = !OpenLeaderBoard;
    }
    public void OpenRemoveAdsPanel()
    {
        if(OpenRemoveAds == true)
        {
            RemoveAds.SetActive(true);
        }
        else
        {
            if(OpenRemoveAds == false)
            {
                RemoveAds.SetActive(false);
            }
        }
        OpenRemoveAds = !OpenRemoveAds;
    }
    public void OpenSettingPanel()
    {
        if(OpenSetting == true)
        {
            PanelPause.SetActive(true);
        }
        else
        {
            if(OpenSetting == false)
            {
                PanelPause.SetActive(false);
            }
        }
        OpenSetting = !OpenSetting;
    }
    public void OpenIapPanel()
    {
        if(OpenIAP == true)
        {
            PanelIAP.SetActive(true);
        }
        else
        {
            if(OpenIAP == false)
            {
                PanelIAP.SetActive(false);
            }
        }
        OpenIAP = !OpenIAP;
    }
    public void selectSkinToy()
    {
        PlayerPrefs.SetString("CurrentCharacter", "roblox_boy");
        PanelSelectSkin.SetActive(false);
        PanelHome.SetActive(true);
    }
    public void selectSkinAplphabit()
    {
        PlayerPrefs.SetString("CurrentCharacter", "roblox_A");
        PanelSelectSkin.SetActive(false);
        PanelHome.SetActive(true);
    }
    public void ManagerName()
    {
        string NamePlayer = InputField.text;
        PlayerPrefs.SetString("PlayerName", NamePlayer);
        PanelName.SetActive(false);
        PanelSelectSkin.SetActive(true);
    }
    IEnumerator CheckingLoadingScreen()
    {
        yield return new WaitForSeconds(2f);
        LoadingScene.SetActive(false);
    }
    IEnumerator LoadingLevel()
    {
        LoadingScene.SetActive(true);
        yield return new WaitForSeconds(2f);
        AdsManager.Instance.RequestInterstitial();
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
    }
}
