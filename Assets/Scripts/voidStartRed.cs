using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class voidStartRed : MonoBehaviour
{
    public GameObject RedMonster;
    public GameObject EffectLanding;
    public Transform Position;
    public GameObject[] CurrentPiles;
    internal int CurrentBattery;
    void Start()
    {
        RedMonster.transform.position = Position.position;
    }
    private void Update()
    {
        if(CurrentBattery == CurrentPiles.Length)
        {
            EffectLanding.SetActive(true);
        }
    }
    public void CheckCurrentPile()
    {
        if(CurrentBattery <= CurrentPiles.Length)
        {
            CurrentPiles[CurrentBattery].gameObject.SetActive(true);
            CurrentBattery += 1;
        }
    }
}
