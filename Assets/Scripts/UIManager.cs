using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEditor;
using UnityEngine.UI;
using UnityEngine;
using UnityEngine.Networking;

public class UIManager : MonoBehaviour
{
    [Header("Manager Controller Animation")]
    public GameObject BarVibration;
    public GameObject BarSound;
    public GameObject BarMusic;

    [Header("Panels Manager")]
    public GameObject PausePanel;

    [Header("Colors Management")]
    public Color NormalColor;
    public Color DarkColor;

    [Header("Buttons Manager")]
    public GameObject BtnVibration;
    public GameObject BtnSound;
    public GameObject BtnMusic;

    [Header("Boolean Manager")]
    internal bool PauseOpen = false;
    internal bool VibrationBool = false;
    internal bool MusicBool = false;
    internal bool SoundBool = false;

    void Start()
    {
        BarMusic.SetActive(false);
        BarSound.SetActive(false);
        BarVibration.SetActive(false);
    }
    void Update()
    {
        
    }
    public void SceneManagemenet(int CurrentScene)
    {
        Time.timeScale = 1;
        SceneManager.LoadScene(CurrentScene);
    }
    public void VibrationBtn()
    {
        VibrationBool = !VibrationBool;
        if(VibrationBool == true)
        {
            BarVibration.SetActive(true);
            BtnVibration.GetComponent<Image>().color = DarkColor;
        }
        else
        {
            BarVibration.SetActive(false);
            BtnVibration.GetComponent<Image>().color = NormalColor;
            Debug.Log("Vibration Its Back");
        }
    }
    public void SoundBtn()
    {
        SoundBool = !SoundBool;
        if(SoundBool == true)
        {
            BarSound.SetActive(true);
            BtnSound.GetComponent<Image>().color = DarkColor;
        }
        else
        {
            if(SoundBool == false)
            {
                BarSound.SetActive(false);
                BtnSound.GetComponent<Image>().color = NormalColor;
                Debug.Log("Sound Its Back");
            }
        }
    }
    public void MusicBtn()
    {
        MusicBool = !MusicBool;
        if(MusicBool == true)
        {
            BarMusic.SetActive(true);
            BtnMusic.GetComponent<Image>().color = DarkColor;
        }
        else
        {
            if(MusicBool == false)
            {
                BarMusic.SetActive(false);
                BtnMusic.GetComponent<Image>().color = NormalColor;
                Debug.Log("Music its Back");
            }
        }
    }
    public void PauseBtn()
    {
        PauseOpen = !PauseOpen;
        if(PauseOpen == true)
        {
            Time.timeScale = 0;
            PausePanel.SetActive(true);
        }
        else
        {
            if(PauseOpen == false)
            {
                PausePanel.SetActive(false);
                Time.timeScale = 1;
            }
        }
    }
}
