using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class PauseManager : MonoBehaviour
{
    [Header("Controlling Panels")]
    public GameObject ChangeName;

    [Header("Controlling PanelChangineName")]
    public TMP_InputField InputName;

    [Header("Controller UI Buttons")]
    public GameObject BarVibration;
    public GameObject BarMusic;
    public GameObject BarSound;

    [Header("Ui Manager Controller")]
    public Image ImageVibration;
    public Image ImageMusic;
    public Image ImageSound;

    [Header("Colors Management")]
    public Color NormalColor;
    public Color DarkColor;

    [Header("Boolean Manager")]
    internal bool VibrationActive = false;
    internal bool MusicActive = false;
    internal bool SoundActive = false;
    internal bool ChangeNamePanel = true;
    public void OpenChangeNamePanel()
    {
        if(ChangeNamePanel == true)
        {
            ChangeName.SetActive(true);
        }
        else
        {
            if(ChangeNamePanel == false)
            {
                PlayerPrefs.SetString("PlayerName", InputName.text);
                ChangeName.SetActive(false);
            }
        }
        ChangeNamePanel = !ChangeNamePanel;
    }
    public void Vibration()
    {
        VibrationActive = !VibrationActive;
        if(VibrationActive == true) 
        {
            BarVibration.SetActive(true);
            ImageVibration.color = DarkColor;
        }
        else
        {
            if(VibrationActive == false)
            {
                BarVibration.SetActive(false);
                ImageVibration.color = NormalColor;
            }
        }
    }
    public void Music()
    {
        MusicActive = !MusicActive;
        if(MusicActive == true)
        {
            BarMusic.SetActive(true);
            ImageMusic.color = DarkColor;
        }
        else
        {
            if(MusicActive == false)
            {
                BarMusic.SetActive(false);
                ImageMusic.color = NormalColor;
            }
        }
    }
    public void Sound()
    {
        SoundActive = !SoundActive;
        if(SoundActive == true)
        {
            BarSound.SetActive(true);
            ImageSound.color = DarkColor;
        }
        else
        {
            if(SoundActive == false)
            {
                BarSound.SetActive(false);
                ImageSound.color = NormalColor;
            }
        }
    }
}
