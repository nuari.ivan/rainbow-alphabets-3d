using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class CheckingMonsterTrigger : MonoBehaviour
{
    public MonsterController ControllerPlayer;
    public GameObject Effects;
    public GameObject ContainerEffect;
    public GameObject CurrentSpriteArrow;
    public float LateTime = 2;

    IEnumerator ControllingSpawning()
    {
        yield return new WaitForSeconds(LateTime);
        (Instantiate(Effects, ContainerEffect.transform.position, Effects.transform.rotation) as GameObject).transform.SetParent(ContainerEffect.transform);
        yield return new WaitForSeconds(0.25f);
        (Instantiate(Effects, ContainerEffect.transform.position, Effects.transform.rotation) as GameObject).transform.SetParent(ContainerEffect.transform);
        yield return new WaitForSeconds(0.25f);
        (Instantiate(Effects, ContainerEffect.transform.position, Effects.transform.rotation) as GameObject).transform.SetParent(ContainerEffect.transform);
        yield return new WaitForSeconds(0.25f);
        (Instantiate(Effects, ContainerEffect.transform.position, Effects.transform.rotation) as GameObject).transform.SetParent(ContainerEffect.transform);
        yield return new WaitForSeconds(0.25f);
        (Instantiate(Effects, ContainerEffect.transform.position, Effects.transform.rotation) as GameObject).transform.SetParent(ContainerEffect.transform);
        yield return new WaitForSeconds(0.25f);
        (Instantiate(Effects, ContainerEffect.transform.position, Effects.transform.rotation) as GameObject).transform.SetParent(ContainerEffect.transform);
        yield return new WaitForSeconds(0.25f);
        (Instantiate(Effects, ContainerEffect.transform.position, Effects.transform.rotation) as GameObject).transform.SetParent(ContainerEffect.transform);
        yield return new WaitForSeconds(0.25f);
        (Instantiate(Effects, ContainerEffect.transform.position, Effects.transform.rotation) as GameObject).transform.SetParent(ContainerEffect.transform);
        yield return new WaitForSeconds(0.25f);
        (Instantiate(Effects, ContainerEffect.transform.position, Effects.transform.rotation) as GameObject).transform.SetParent(ContainerEffect.transform);
    }
    void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            Debug.Log("Player Has Been Dead");
            ControllerPlayer.CameraOver.SetActive(true);
            ControllerPlayer.Character.SetActive(true);
            CurrentSpriteArrow.GetComponent<SpriteRenderer>().enabled = false;
            ControllerPlayer.gameObject.GetComponent<NavMeshAgent>().enabled = false;
            ControllerPlayer.ManagerDead();
            ControllerPlayer.GameManager.PlayerManaegr.gameObject.SetActive(false);
            ControllerPlayer.GameManager.GameOver = true;
            StartCoroutine(ControllingSpawning());
            ControllerPlayer.GameOver = true;
            ControllerPlayer.HitPlayerAnim = true;
            other.GetComponent<PlayerController>().ControllerLancher.GameFinish();
        }
        if (other.CompareTag("CharacterFollow"))
        {
            other.GetComponent<CharactersMoving>().LeaderController.DeadIcon.SetActive(true);
            other.GetComponent<CharactersMoving>().LeaderController.playerName[0].gameObject.SetActive(false);
            other.GetComponent<CharactersMoving>().LeaderController.playerName[1].gameObject.SetActive(true);
            Destroy(other.gameObject);
            Debug.Log("Enemey" + other.gameObject.name);
        }
    }
}
