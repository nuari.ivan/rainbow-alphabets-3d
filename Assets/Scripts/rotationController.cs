using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class rotationController : MonoBehaviour
{
    public Vector3 rotation;

    void Update()
    {
        transform.rotation = Quaternion.Euler(rotation.x, rotation.y, rotation.z);
        transform.localScale = new Vector3(0.05f, 0.05f, 0.05f);
    }
}
