using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PowerUpBoxes : MonoBehaviour
{
    public PlayerController Player;

    public void ManagerPlayer()
    {
        Player.TakeMoreBox = 1;
    }

    public void WatchAds(string tipe)
    {
        AdsManager.Instance.RequestRewardedAds(tipe);
    }

    private void OnEnable()
    {
        AdsManager.Instance.OnAdsRewarded += Instance_OnAdsRewarded;
    }

    private void OnDisable()
    {
        AdsManager.Instance.OnAdsRewarded -= Instance_OnAdsRewarded;
    }
    private void Instance_OnAdsRewarded(string placement)
    {
        if (placement == "time")
        {

        }
        if (placement == "boxes")
        {
            ManagerPlayer();
        }
        if (placement == "speed")
        {

        }
    }
}
