using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;
using TMPro;

public class IapTicket : MonoBehaviour
{
    [Header("Price Controller UI")]
    public TextMeshProUGUI CurrentPrice;

    [Header("Integer Manager")]
    internal float CurrentPriceValue;

    void Start()
    {
        CurrentPrice.text = "RP. " + CurrentPriceValue;
    }
    public void ButtonBuy()
    {
        Debug.Log("" + -1 * CurrentPriceValue);
        CurrentPriceValue -= CurrentPriceValue;
    }
}