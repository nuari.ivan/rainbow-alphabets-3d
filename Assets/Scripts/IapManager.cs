using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IapManager : MonoBehaviour
{
    [Header("All Tickets")]
    public IapTicket[] Tickiets;

    void Awake()
    {
        Ticket1();
        Ticket2();
        Ticket3();
        Ticket4();
        Ticket5();
        Ticket6();
    }

    private void OnEnable()
    {
        AdsManager.Instance.OnAdsRewarded += Instance_OnAdsRewarded;
    }

    private void OnDisable()
    {
        AdsManager.Instance.OnAdsRewarded -= Instance_OnAdsRewarded;
    }

    private void Instance_OnAdsRewarded(string placement)
    {
        // saat reward ads diberikan selesai apa yang akan didapat player tulis kodenya dibawah ini!
        if(placement == "DoubleIap")
        {
            int money = PlayerPrefs.GetInt("CurrentMoney", 0);
            money += 500;
            PlayerPrefs.SetInt("CurrentMoney", money);
        }
    }

    public void WatchAds()
    {
        AdsManager.Instance.RequestRewardedAds("DoubleIap");
    }
    void Ticket1()
    {
        Tickiets[0].CurrentPriceValue = 10.000f;
    }
    void Ticket2()
    {
        Tickiets[1].CurrentPriceValue = 20.000f;
    }
    void Ticket3()
    {
        Tickiets[2].CurrentPriceValue = 30.000f;
    }
    void Ticket4()
    {
        Tickiets[3].CurrentPriceValue = 50.000f;
    }
    void Ticket5()
    {
        Tickiets[4].CurrentPriceValue = 70.000f;
    }
    void Ticket6()
    {
        Tickiets[5].CurrentPriceValue = 100.000f;
    }
}
