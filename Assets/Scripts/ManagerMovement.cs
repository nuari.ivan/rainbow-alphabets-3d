using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ManagerMovement : MonoBehaviour
{
    public GameObject OtherPosition;
    public GameObject CurrentCharacter;
    internal bool SpawnOther = false;
    internal bool MoveCharacterUp = false;
    internal bool MoveCharacterDown = false;
    internal float ValeurX = 0;
    internal float ValeurY = 0;
    void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            if(SpawnOther == true)
            {
                CurrentCharacter = other.gameObject;
                CurrentCharacter.GetComponent<PlayerController>().BodyAnimation.Play("JumpAnimation");
                CurrentCharacter.GetComponent<PlayerController>().SRC.clip = CurrentCharacter.GetComponent<PlayerController>().LaughingSND;
                CurrentCharacter.GetComponent<PlayerController>().SRC.Play();
                StartCoroutine(JumpPlayer());
            }
        }
    }
    IEnumerator JumpPlayer()
    {
        yield return new WaitForSeconds(0.45f);
        CurrentCharacter.GetComponent<Rigidbody>().isKinematic = false;
        CurrentCharacter.transform.position = new Vector3(OtherPosition.transform.position.x, CurrentCharacter.transform.position.y, OtherPosition.transform.position.z);
    }
    void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            SpawnOther = true;
            StartCoroutine(WaitingBackValue());
        }
    }
    IEnumerator WaitingBackValue()
    {
        yield return new WaitForSeconds(4f);
        SpawnOther = false;
    }
}
