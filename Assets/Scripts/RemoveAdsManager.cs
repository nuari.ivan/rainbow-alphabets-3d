using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RemoveAdsManager : MonoBehaviour
{
    [Header("Current PopUp")]
    public GameObject CurrentP;
    public GameObject CurrentButton;

    [Header("Boolean manager")]
    internal bool ButtonUnlocked = false;

    public void RemoveAds()
    {
        if(ButtonUnlocked == true)
        {
            CurrentP.SetActive(false);
            CurrentButton.SetActive(false);
        }
    }
}
