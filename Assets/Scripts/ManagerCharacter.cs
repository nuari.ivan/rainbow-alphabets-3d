using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ManagerCharacter : MonoBehaviour
{
    [Header("Manager Objects Controller")]
    public GameObject CurrentEnemy;

    [Header("Controlling Manager")]
    public MonsterController MonsterManager;
    void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("CharacterFollow"))
        {
            MonsterManager.CustomObject = other.gameObject;
            MonsterManager.PlayerTrigged = true;
            MonsterManager.WalkingAnim = false;
            MonsterManager.RuningAnim = true;
        }
        if (other.CompareTag("Player"))
        {
            MonsterManager.CustomObject = other.gameObject;
            MonsterManager.PlayerTrigged = true;
            MonsterManager.WalkingAnim = false;
            MonsterManager.RuningAnim = true;
        }
    }
    void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            MonsterManager.PlayerTrigged = false;
            MonsterManager.WalkingAnim = true;
            MonsterManager.RuningAnim = false;
        }
        if (other.CompareTag("CharacterFollow"))
        {
            MonsterManager.PlayerTrigged = false;
        }
    }
}
