using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

public class instanceDestroy : MonoBehaviour
{
    void OnEnable()
    {
        StartCoroutine(Destroy());
    }
    void OnDisable()
    {
        StopCoroutine(Destroy());
    }
    IEnumerator Destroy()
    {
        yield return new WaitForSeconds(1.85f);
        Destroy(this.gameObject);
    }
}
