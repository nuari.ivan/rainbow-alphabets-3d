using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityEngine;
using TMPro;

public class GameLauncherController : MonoBehaviour
{
    [Header("Manager Controller")]
    public PlayerController PlayerManaegr;
    public voiStart ManagerCurrentStartWin;
    public BooleanManager BoolM;
    public voidStartFoal ManagerLight;
    public GameObject[] MovingCh;

    [Header("All Boxes")]
    public GameObject[] Lamps;
    public GameObject[] Battery;
    public GameObject[] AllLeds;
    public GameObject[] AllFoods;
    public GameObject[] AllBoxes;
    public GameObject[] ActiveBoxes;
    public GameObject[] PositionBoxes;
    public GameObject[] LeaderBoard;
    public GameObject[] AllObjects;
    public GameObject[] ObjectGameOver;
    public GameObject[] GamePanel;
    public GameObject[] Characters;
    public GameObject[] ListPositionCharacter;


    [Header("GameObjects")]
    public GameObject GameStartTimer;
    public GameObject GameOverPanel;
    public GameObject GameWinPanel;
    public GameObject ContainerObj;
    public GameObject ObjectGamePlay;
    public GameObject PowerUpPanel;

    [Header("Hand Manager")]
    public GameObject ManagerTutorialHand;

    [Header("Game Over Finish")]
    public GameObject CamraMain;

    [Header("Win panel Management")]
    public TextMeshProUGUI CurrentValeurCoins;
    public Slider CurrentSliderAnimation;

    [Header("Integer Manager")]
    internal int AllInt;
    internal int CurrentActiveBox = 0;
    internal int CountDown = 4;
    internal int CurrentLeaderBoard = 0;
    internal int TotalBoxTakeit = 0;
    internal int CurrenCash;
    internal int AACharacter;
    internal int ManagerMultipleCash = 0;internal int CurrentCharacterList = 0;internal int CurrentPositionCharacters = 0;

    [Header("UI Manager")]
    public TextMeshProUGUI CurrentBoxes;
    public TextMeshProUGUI CurrentCount;

    [Header("Boolean Manager")]
    internal bool TimesUp = false;
    internal bool BoxHiddenActive = false;
    internal bool FixingOpenBox = true;
    internal bool FixNumber = true;
    internal bool CheckGameAt = true;
    internal bool GameStart = false;
    internal bool GameOver = false;
    internal bool GameWin = false;
    internal bool FixingGameOver = true;
    internal bool FixingSliderMoving = true;
    void Start()
    {
        ManagerBoxesActivation();
        StartCoroutine(CheckingStart());
        StartCoroutine(CheckingHand());
        StartCoroutine(PlayerSpawningCharacter());
        StartCoroutine(StartChecking());
        ManagerTutorialHand.SetActive(true);
    }
    void Update()
    {
        ManagerWinGame();
        ActiveBoxes = GameObject.FindGameObjectsWithTag("Box");
        if (FixNumber == true)
        {
            AllInt = ActiveBoxes.Length;
            FixNumber = false;
        }
        if(GameOver == true)
        {
            if(FixingGameOver == true)
            {
                Debug.Log("You are Dead");
                StartCoroutine(CheckingLosingPlayer());
                FixingGameOver = false;
            }
        }
        CurrentBoxes.text = ActiveBoxes.Length + "/" + AllInt;
    }
    IEnumerator StartChecking()
    {
        yield return new WaitForSeconds(2);
        PowerUpPanel.SetActive(true);
        yield return new WaitForSeconds(2f);
        //PowerUpPanel.SetActive(false);
    }
    IEnumerator CheckingHand()
    {
        yield return new WaitForSeconds(2f);
        ManagerTutorialHand.SetActive(false);
    }
    public void GameFinish()
    {
        CamraMain.SetActive(false);
    }
    void ManagerWinGame()
    {
        if(GameWin == true)
        {
            foreach(GameObject AA in AllObjects)
            {
                AA.SetActive(false);
            }
            Time.timeScale = 0;
            GameWinPanel.gameObject.SetActive(true);
            CurrenCash = TotalBoxTakeit * 3;
            if(CurrentSliderAnimation.value == 0)
            {
                FixingSliderMoving = true;
            }
            else
            {
                if(CurrentSliderAnimation.value == 1)
                {
                    FixingSliderMoving = false;
                }
            }
            
            if(FixingSliderMoving == true)
            {
                CurrentSliderAnimation.value += Time.unscaledDeltaTime * 1.5f;
            }
            else
            {
                if(FixingSliderMoving == false)
                {
                    CurrentSliderAnimation.value -= Time.unscaledDeltaTime * 1.5f;
                }
            }
            CurrentValeurCoins.text = CurrenCash * (int)(CurrentSliderAnimation.value * 100) + "k";
            ManagerCurrentStartWin.CurrentCash = CurrenCash * (int)(CurrentSliderAnimation.value * 100);
        }
    }
    public void WatchReward()
    {

    }

    public void ManagerBoxesActivation()
    {
        foreach(GameObject AA in PositionBoxes)
        {
            if(CheckGameAt == true)
            {
                AA.SetActive(false);
            }
        }
        PositionBoxes[CurrentActiveBox].SetActive(true);
    }
    public void ActiveBox()
    {
        BoxHiddenActive = !BoxHiddenActive;
        if(BoxHiddenActive == true)
        {
            foreach (Animator CharacterAnimation in PlayerManaegr.AllAnimation)
            {
                if (CharacterAnimation.gameObject.activeSelf == true)
                {
                    if (FixingOpenBox == true)
                    {
                        PlayerManaegr.CharacterRunner = false;
                        CharacterAnimation.Play("open_box");
                        StartCoroutine(WaitingBoxActive());
                        FixingOpenBox = false;
                    }
                }
            }
        }
        else
        {
            if(BoxHiddenActive == false)
            {
                PlayerManaegr.BoxRunner = false;
                PlayerManaegr.CharacterRunner = false;
                FixingOpenBox = true;
                foreach (Animator CharacterAnimation in PlayerManaegr.AllAnimation)
                {
                    if (CharacterAnimation.gameObject.activeSelf == true)
                    {
                        CharacterAnimation.Play("close_box");
                        StartCoroutine(GobackBox());
                    }
                }
            }
        }
    }
    IEnumerator CheckingLosingPlayer()
    {
        yield return new WaitForSeconds(6f);
        //Time.timeScale = 0;
        foreach (GameObject AA in ObjectGameOver)
        {
            AA.SetActive(false);
            GameOverPanel.SetActive(true);
            ObjectGamePlay.SetActive(false);
        }
    }
    IEnumerator PlayerSpawningCharacter()
    {
        yield return new WaitForSeconds(0.25f);
        CurrentCharacterList = Random.Range(0, Characters.Length);
        CurrentPositionCharacters = Random.Range(0, ListPositionCharacter.Length);
        if(AACharacter < 15)
        {
            (Instantiate(Characters[CurrentCharacterList], ListPositionCharacter[CurrentPositionCharacters].transform.position, Characters[CurrentCharacterList].transform.rotation) as GameObject).transform.SetParent(ContainerObj.transform);
            AACharacter += 1;
        }
        StartCoroutine(PlayerSpawningCharacter());
    }
    IEnumerator CheckingStart()
    {
        CountDown -= 1;
        MovingCh = GameObject.FindGameObjectsWithTag("CharacterFollow");
        CurrentCount.text = "" + CountDown;
        yield return new WaitForSeconds(1f);
        MovingCh = GameObject.FindGameObjectsWithTag("CharacterFollow");
        CountDown -= 1;
        CurrentCount.text = "" + CountDown;
        yield return new WaitForSeconds(1f);
        MovingCh = GameObject.FindGameObjectsWithTag("CharacterFollow");
        CountDown -= 1;
        CurrentCount.text = "" + CountDown;
        yield return new WaitForSeconds(1f);
        MovingCh = GameObject.FindGameObjectsWithTag("CharacterFollow");
        CountDown -= 1;
        CurrentCount.text = "" + CountDown;
        yield return new WaitForSeconds(0.5f);
        MovingCh = GameObject.FindGameObjectsWithTag("CharacterFollow");
        foreach (GameObject AA in MovingCh)
        {
            AA.gameObject.GetComponent<CharactersMoving>().CheckMovePP = true;
            GameStart = true;
        }
        foreach(GameObject ActivateObjects in AllObjects)
        {
            ActivateObjects.SetActive(true);
        }
        StartCoroutine(StartLeaderBoarding());
        GameStartTimer.SetActive(false);
    }
    IEnumerator StartLeaderBoarding()
    {
        yield return new WaitForSeconds(0.1f);
        if(CurrentLeaderBoard < LeaderBoard.Length)
        {
            LeaderBoard[CurrentLeaderBoard].SetActive(true);
            CurrentLeaderBoard += 1;
        }
        StartCoroutine(StartLeaderBoarding());
    }
    IEnumerator GobackBox()
    {
        yield return new WaitForSeconds(0.6f);
        PlayerManaegr.BoxRunner = false;
        PlayerManaegr.CharacterRunner = true;
    }
    IEnumerator WaitingBoxActive()
    {
        yield return new WaitForSeconds(0.6f);
        PlayerManaegr.BoxRunner = true;
        PlayerManaegr.CharacterRunner = false;
    }
}
