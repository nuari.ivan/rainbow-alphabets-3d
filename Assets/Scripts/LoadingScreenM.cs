using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityEngine;

public class LoadingScreenM : MonoBehaviour
{
    [Header("Controlling UI")]
    public Image LoadingBar;

    [Header("Floating Controller")]
    public float LoadingTime;

    [Header("Boolean Manager")]
    internal bool CheckTimefinish = true;
    void Awake()
    {
        Time.timeScale = 1f;
    }
    void Update()
    {
        if(CheckTimefinish == true)
        {
            LoadingBar.fillAmount += 1 / LoadingTime * Time.deltaTime;
            if(LoadingBar.fillAmount == 1)
            {
                AdsManager.Instance.RequestInterstitial();

                SceneManager.LoadScene(1);
                CheckTimefinish = false;
            }
        }
    }
}
