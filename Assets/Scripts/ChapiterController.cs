using UnityEngine;


public class ChapiterController : MonoBehaviour
{
    [Header("Manager Controller")]
    public GameLauncherController Launcher;
    public BooleanManager BoolM;

    [Header("Doors Componenet")]
    public GameObject[] ChapiterOneDoor;
    public GameObject[] ChapiterTwoDoor;
    public GameObject[] ChapiterThreeDoor;
    public GameObject[] ChapiterFourDoor;
    public GameObject[] ChapiterFiveDoor;
    public GameObject[] ChapiterSixDoor;

    [Header("Manager Monster Bt Level")]
    public GameObject[] AllMonsters;

    [Header("Floating Manager")]
    internal float ManagerPositionY = 0;


    void Start()
    {

    }
    void Update()
    {
        if(Launcher.GameStart == true)
        {
            if (BoolM.LevelOne == true)
            {
                ManagerPositionY -= Time.deltaTime * 2;
                foreach (GameObject AA in ChapiterOneDoor)
                {
                    AA.transform.position = new Vector3(AA.transform.position.x, ManagerPositionY, AA.transform.position.z);
                }
            }
            if(BoolM.LevelTwo == true)
            {
                ManagerPositionY -= Time.deltaTime * 2;
                foreach (GameObject BB in ChapiterTwoDoor)
                {
                    BB.transform.position = new Vector3(BB.transform.position.x, ManagerPositionY, BB.transform.position.z);
                }
            }
            if(BoolM.LevelThree == true)
            {
                ManagerPositionY -= Time.deltaTime * 2;
                foreach (GameObject CC in ChapiterThreeDoor)
                {
                    CC.transform.position = new Vector3(CC.transform.position.x, ManagerPositionY, CC.transform.position.z);
                }
            }
            if (BoolM.LevelFour == true)
            {
                ManagerPositionY -= Time.deltaTime * 2;
                foreach (GameObject CC in ChapiterFourDoor)
                {
                    CC.transform.position = new Vector3(CC.transform.position.x, ManagerPositionY, CC.transform.position.z);
                }
            }
            if (BoolM.LevelFive == true)
            {
                ManagerPositionY -= Time.deltaTime * 2;
                foreach (GameObject CC in ChapiterFiveDoor)
                {
                    CC.transform.position = new Vector3(CC.transform.position.x, ManagerPositionY, CC.transform.position.z);
                }
            }
        }
    }
}
