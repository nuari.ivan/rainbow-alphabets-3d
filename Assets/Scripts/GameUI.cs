using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameUI : MonoBehaviour
{
    [Header("Manager Controller")]
    public BooleanManager BoolM;

    [Header("UI Manager")]
    public GameObject NightUI;

    void Start()
    {
        if(BoolM.LevelFour == true)
        {
            NightUI.SetActive(true);
        }
        else
        {
            NightUI.SetActive(false);
        }
    }
}
