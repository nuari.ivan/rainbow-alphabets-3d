using System.Collections;
using System.Collections.Generic;
using UnityEngine.AI;
using UnityEngine.UI;
using UnityEngine;
using TMPro;

public class MonsterController : MonoBehaviour
{
    [Header("Manager GamePlay")]
    public GameLauncherController GameManager;

    [Header("Controlling AI Manager")]
    private NavMeshAgent CurrentAI;

    [Header("Animation Player Over")]
    public GameObject CameraOver;
    public GameObject Character;

    [Header("Compoenet Characters")]
    public GameObject[] ListCharacters;

    [Header("Manager Controller Animator")]
    public Animator CurrentAnimator;

    [Header("Rigidbody Physics")]
    public Rigidbody ThisRb;

    [Header("Current Players")]
    public GameObject[] AllCharacters;
    public GameObject CustomObject;

    [Header("Floating Manager")]
    internal float ManagerDIstance = 0f;
    public float NormalSpeed = 3.5f;
    public float SpeedAdvanced = 5.5f;

    [Header("Integer Manager")]
    internal int CurrentCharacterFollow = 3;

    [Header("Boolean manager")]
    internal bool CheckIfPlayerMoving = true;
    internal bool WalkingAnim = true;
    internal bool RuningAnim = false;
    internal bool IdleAnim = false;
    internal bool HitPlayerAnim = false;
    internal bool PlayerTrigged = false;
    internal bool GameOver = false;

    void Start()
    {
        GameManager = GameObject.Find("GameLauncher").gameObject.GetComponent<GameLauncherController>();
        CurrentAI = this.gameObject.GetComponent<NavMeshAgent>();
        ThisRb = this.gameObject.GetComponent<Rigidbody>();
    }
    void Update()
    {

    }
    internal void ManagerDead()
    {
        foreach (GameObject Character in ListCharacters)
        {
            if (PlayerPrefs.GetString("CurrentCharacter") == Character.name)
            {
                Character.SetActive(true);
                Character.GetComponent<Animator>().enabled = false;
            }
        }
    }
    void FixedUpdate()
    {
        if(GameOver == false)
        {
            if (WalkingAnim == true && GameManager.GameStart == true)
            {
                CurrentAnimator.Play("walk");
                CurrentAI.speed = NormalSpeed;
            }
            if (RuningAnim == true && GameManager.GameStart == true)
            {
                CurrentAnimator.Play("run");
                CurrentAI.speed = SpeedAdvanced;
            }
            if (IdleAnim == true)
            {
                CurrentAnimator.Play("idle");
            }
        }
        if(HitPlayerAnim == true)
        {
            CurrentAnimator.Play("roar");
            HitPlayerAnim = false;
        }
        ControllerManagerFollowing();

    }
    void ControllerManagerFollowing()
    {
        if(PlayerTrigged == false && GameOver == false)
        {
            AllCharacters = GameObject.FindGameObjectsWithTag("CharacterFollow");
            if (CheckIfPlayerMoving == true && AllCharacters.Length > 0 && GameManager.GameStart == true)
            {
                if (CurrentCharacterFollow >= 0)
                {
                    ManagerDIstance = Vector3.Distance(transform.position, AllCharacters[0].transform.position);
                    CurrentAI.SetDestination(AllCharacters[0].transform.position);
                }
            }
        }
        else
        {
            if(CustomObject != null && GameOver == false)
            CurrentAI.SetDestination(CustomObject.transform.position);
        }
    }
}
