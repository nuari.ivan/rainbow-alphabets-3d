using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityEngine;

public class BooleanManager : MonoBehaviour
{
    [Header("Levels Manager")]
    public GameObject[] PositionMonsters;
    public GameObject[] Monsters;
    public GameObject[] InactivatePanels;
    public GameObject[] SpawningAeas;

    [Header("Secondair Objects")]
    public GameObject MachineFood;
    public GameObject MachineLed;
    public GameObject MonsterGreen;
    public GameObject MachineBattery;
    public GameObject MachineSanta;
    
    [Header("Boolean Manager")]
    public bool LevelOne = false;
    public bool LevelTwo = false;
    public bool LevelThree = false;
    public bool LevelFour = false;
    public bool LevelFive = false;
    public bool LevelSix = false;
    void Awake()
    {
        ManagerController();
    }
    void ManagerController()
    {
        if(LevelOne == true)
        {
            (Instantiate(Monsters[0], PositionMonsters[0].transform.position, Monsters[0].transform.rotation) as GameObject).transform.SetParent(transform);
            SpawningAeas[0].SetActive(true);
        }
        if(LevelTwo == true)
        {
            InactivatePanels[0].SetActive(false);
            MachineFood.SetActive(true);
            (Instantiate(Monsters[1], PositionMonsters[1].transform.position, Monsters[1].transform.rotation) as GameObject).transform.SetParent(transform);
            SpawningAeas[1].SetActive(true);
        }
        if(LevelThree == true)
        {
            InactivatePanels[1].SetActive(false);
            InactivatePanels[0].SetActive(false);
            MachineLed.SetActive(true);
            (Instantiate(Monsters[2], PositionMonsters[2].transform.position, Monsters[2].transform.rotation) as GameObject).transform.SetParent(transform);
            SpawningAeas[2].SetActive(true);
        }
        if(LevelFour == true)
        {
            InactivatePanels[1].SetActive(false);
            InactivatePanels[0].SetActive(false);
            MonsterGreen.SetActive(true);
            MachineBattery.SetActive(true);
            SpawningAeas[3].SetActive(true);
        }
        if(LevelFive == true)
        {
            InactivatePanels[1].SetActive(false);
            InactivatePanels[0].SetActive(false);
            (Instantiate(Monsters[3], PositionMonsters[3].transform.position, Monsters[3].transform.rotation) as GameObject).transform.SetParent(transform);
            SpawningAeas[4].SetActive(true);
            MachineSanta.SetActive(true);
        }
        if(LevelSix == true)
        {
            (Instantiate(Monsters[3], PositionMonsters[3].transform.position, Monsters[3].transform.rotation) as GameObject).transform.SetParent(transform);
        }
    }
}
