using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoxSpawning : MonoBehaviour
{
    [Header("Manager Controller")]
    public GameLauncherController Launcher;
    public BooleanManager BoolM;
    public GameObject Container;

    [Header("Integer Manager")]
    internal int CurrentBox = 0;


    void Start()
    {
        if(BoolM.LevelOne == true)
        {
            CurrentBox = Random.Range(0, Launcher.AllBoxes.Length);
            (Instantiate(Launcher.AllBoxes[CurrentBox], new Vector3(transform.position.x, transform.position.y + 1, transform.position.z), Launcher.AllBoxes[CurrentBox].transform.rotation) as GameObject).transform.SetParent(Container.transform);
        }
        if(BoolM.LevelTwo == true)
        {
            CurrentBox = Random.Range(0, Launcher.AllFoods.Length);
            (Instantiate(Launcher.AllFoods[CurrentBox], new Vector3(transform.position.x, transform.position.y + 1, transform.position.z), Launcher.AllFoods[CurrentBox].transform.rotation) as GameObject).transform.SetParent(Container.transform);
        }
        if (BoolM.LevelThree == true)
        {
            CurrentBox = Random.Range(0, Launcher.AllLeds.Length);
            (Instantiate(Launcher.AllLeds[CurrentBox], new Vector3(transform.position.x, transform.position.y + 1, transform.position.z), Launcher.AllLeds[CurrentBox].transform.rotation) as GameObject).transform.SetParent(Container.transform);
        }
        if (BoolM.LevelFour == true)
        {
            CurrentBox = Random.Range(0, Launcher.Battery.Length);
            (Instantiate(Launcher.Battery[CurrentBox], new Vector3(transform.position.x, transform.position.y + 1, transform.position.z), Launcher.Battery[CurrentBox].transform.rotation) as GameObject).transform.SetParent(Container.transform);
        }
        if (BoolM.LevelFive == true)
        {
            CurrentBox = Random.Range(0, Launcher.Lamps.Length);
            (Instantiate(Launcher.Lamps[CurrentBox], new Vector3(transform.position.x, transform.position.y, transform.position.z), Launcher.Lamps[CurrentBox].transform.rotation) as GameObject).transform.SetParent(Container.transform);
        }
    }
}
