using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class triggerChecker : MonoBehaviour
{
    public MonsterMovementGreen MonsterGreen;
    void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            MonsterGreen.PlayerCatched = true;
        }
        if (other.CompareTag("CharacterFollow"))
        {
            other.GetComponent<CharactersMoving>().LeaderController.DeadIcon.SetActive(true);
            other.GetComponent<CharactersMoving>().LeaderController.playerName[0].gameObject.SetActive(false);
            other.GetComponent<CharactersMoving>().LeaderController.playerName[1].gameObject.SetActive(true);
            Destroy(other.gameObject);
            Debug.Log("Enemey" + other.gameObject.name);
        }
    }
}
