using GoogleMobileAds.Api;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AdsManager : MonoBehaviour
{
    public static AdsManager Instance;

    private BannerView banner;
    private InterstitialAd interstitial;
    private RewardedAd rewarded;

    [SerializeField] private string banner_id;
    [SerializeField] private string interstitial_id;
    [SerializeField] private string rewarded_id;

    public delegate void GetRewardDelegate(string placement);
    public event GetRewardDelegate OnAdsRewarded;

    private void Awake()
    {
        if(Instance == null)
        {
            Instance = this;
        }
        else
        {
            Destroy(gameObject);
        }
        DontDestroyOnLoad(gameObject);
    }

    private void Start()
    {
        MobileAds.RaiseAdEventsOnUnityMainThread = true;

        InitializeAds();
    }

    private void InitializeAds()
    {
        MobileAds.Initialize((InitializationStatus status) =>
        {

        });

        LoadBannerAds();
        LoadInterstitialAds();
        LoadRewardedAds();
    }

    private void LoadRewardedAds()
    {
        if (rewarded != null)
        {
            rewarded.Destroy();
            rewarded = null;
        }

        var adRequest = new AdRequest();
        adRequest.Keywords.Add("my-admob-ads");

        RewardedAd.Load(rewarded_id, adRequest,
            (RewardedAd ad, LoadAdError error) =>
            {
                if (error != null || ad == null)
                {
                    Debug.LogError("Rewarded ad failed to load an ad " +
                                   "with error : " + error);
                    return;
                }

                Debug.Log("Rewarded ad loaded with response : "
                          + ad.GetResponseInfo());

                rewarded = ad;

                SetRewardedEvents();
            });
    }

    private void LoadInterstitialAds()
    {
        if (interstitial != null)
        {
            interstitial.Destroy();
            interstitial = null;
        }

        var adRequest = new AdRequest();
        adRequest.Keywords.Add("my-admob-ads");

        InterstitialAd.Load(interstitial_id, adRequest,
            (InterstitialAd ad, LoadAdError error) =>
            {
                if (error != null || ad == null)
                {
                    Debug.LogError("interstitial ad failed to load an ad " +
                                   "with error : " + error);
                    return;
                }

                Debug.Log("Interstitial ad loaded with response : "
                          + ad.GetResponseInfo());

                interstitial = ad;

                SetInterstitialEvents();
            });
    }

    private void LoadBannerAds()
    {
        if (banner != null)
        {
            banner.Destroy();
            banner = null;
        }

        var adRequest = new AdRequest();
        adRequest.Keywords.Add("my-admob-ads");

        banner = new BannerView(banner_id, AdSize.Banner, AdPosition.Bottom);

        banner.LoadAd(adRequest);
    }

    public void RequestRewardedAds(string placement)
    {
        if (rewarded.CanShowAd())
        {
            rewarded.Show(OnGetReward);
            OnAdsRewarded?.Invoke(placement);
        }
    }

    private void OnGetReward(Reward newReward)
    {

    }

    public void RequestInterstitial()
    {
        if (interstitial.CanShowAd())
        {
            interstitial.Show();
        }
    }

    public void RequestBanner()
    {
        if (banner != null)
        {
            banner.Show();
        }
    }

    private void SetInterstitialEvents()
    {
        interstitial.OnAdClicked += Interstitial_OnAdClicked;
        interstitial.OnAdFullScreenContentClosed += Interstitial_OnAdFullScreenContentClosed;
        interstitial.OnAdFullScreenContentFailed += Interstitial_OnAdFullScreenContentFailed;
        interstitial.OnAdFullScreenContentOpened += Interstitial_OnAdFullScreenContentOpened;
        interstitial.OnAdImpressionRecorded += Interstitial_OnAdImpressionRecorded;
        interstitial.OnAdPaid += Interstitial_OnAdPaid;
    }


    private void SetRewardedEvents()
    {
        rewarded.OnAdClicked += Rewarded_OnAdClicked;
        rewarded.OnAdFullScreenContentClosed += Rewarded_OnAdFullScreenContentClosed;
        rewarded.OnAdFullScreenContentFailed += Rewarded_OnAdFullScreenContentFailed;
        rewarded.OnAdFullScreenContentOpened += Rewarded_OnAdFullScreenContentOpened;
        rewarded.OnAdImpressionRecorded += Rewarded_OnAdImpressionRecorded;
        rewarded.OnAdPaid += Rewarded_OnAdPaid;
    }

    private void OnDisable()
    {
        interstitial.OnAdClicked -= Interstitial_OnAdClicked;
        interstitial.OnAdFullScreenContentClosed -= Interstitial_OnAdFullScreenContentClosed;
        interstitial.OnAdFullScreenContentFailed -= Interstitial_OnAdFullScreenContentFailed;
        interstitial.OnAdFullScreenContentOpened -= Interstitial_OnAdFullScreenContentOpened;
        interstitial.OnAdImpressionRecorded -= Interstitial_OnAdImpressionRecorded;
        interstitial.OnAdPaid -= Interstitial_OnAdPaid;

        rewarded.OnAdClicked -= Rewarded_OnAdClicked;
        rewarded.OnAdFullScreenContentClosed -= Rewarded_OnAdFullScreenContentClosed;
        rewarded.OnAdFullScreenContentFailed -= Rewarded_OnAdFullScreenContentFailed;
        rewarded.OnAdFullScreenContentOpened -= Rewarded_OnAdFullScreenContentOpened;
        rewarded.OnAdImpressionRecorded -= Rewarded_OnAdImpressionRecorded;
        rewarded.OnAdPaid -= Rewarded_OnAdPaid;
    }

    private void Rewarded_OnAdClicked()
    {
    }

    private void Rewarded_OnAdPaid(AdValue obj)
    {
    }

    private void Rewarded_OnAdImpressionRecorded()
    {
    }

    private void Rewarded_OnAdFullScreenContentFailed(AdError obj)
    {
    }

    private void Rewarded_OnAdFullScreenContentClosed()
    {
    }

    private void Rewarded_OnAdFullScreenContentOpened()
    {
    }

    private void Interstitial_OnAdPaid(AdValue obj)
    {
    }

    private void Interstitial_OnAdImpressionRecorded()
    {
    }

    private void Interstitial_OnAdFullScreenContentOpened()
    {
    }

    private void Interstitial_OnAdFullScreenContentFailed(AdError obj)
    {
    }

    private void Interstitial_OnAdFullScreenContentClosed()
    {
    }

    private void Interstitial_OnAdClicked()
    {
    }
}
