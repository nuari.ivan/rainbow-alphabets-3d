using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Experimental.GlobalIllumination;
using UnityEngine.UI;

public class controllingCharacters : MonoBehaviour
{
    [Header("Manager Controller")]
    public ShopSkinsManager ManagerSkins;

    [Header("Controlling UI")]
    public GameObject LockBg;
    public GameObject CheckBox;
    
    [Header("Manager this GameObject")]
    private Button CurrentBtn;

    [Header("strings Manager")]
    public string CurrentTag;

    [Header("Integer Manager")]
    public int CurrentNum;

    [Header("Boolean Manager")]
    public bool ObjectLocked = true;

    void Start()
    {
        CurrentBtn = this.gameObject.GetComponent<Button>();
        CurrentBtn.onClick.AddListener(ClickableBtn);
    }
    void ClickableBtn()
    {
        if(ObjectLocked == false)
        {
            ManagerSkins.CurrentCharacter = CurrentNum;
            ManagerSkins.ManagerCurrentCharacter();
            LockBg.SetActive(false);
            CheckBox.SetActive(true);
            PlayerPrefs.SetString("CurrentCharacter", CurrentTag);
        }
    }
}
