using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class voistartController : MonoBehaviour
{
    public GameObject MonsterRed;
    public Transform Position;

    public GameObject Effects;

    public Animator Blue;
    public Animator Red;
    public Animator Yellow;

    [Header("Floating Manager")]
    public float ValueBlue = 0.05f;
    public float ValueYellow = 0.05f;
    public float ValueRed = 0.05f;
    void Start()
    {
        MonsterRed.transform.position = Position.position;
    }
    private void Update()
    {
        if(ValueBlue >= 0.5f && ValueRed >= 0.5f)
        {
            Effects.SetActive(true);
        }
        Blue.Play("machine", -1, ValueBlue);
        Red.Play("machine", -1, ValueRed);
        Yellow.Play("machine", -1, ValueYellow);
    }
}
